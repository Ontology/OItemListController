﻿using OItemListController.Services;
using OItemListController.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace OItemListController.Controllers
{
    public class OntoListController : OntoListViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private ServiceAgent_ElasticOItemList serviceAgent_Elastic;

        private clsLocalConfig localConfig;

        private string itemFileName;

        private List<string> channelsToAdd;
        private List<string> channelsToRemove;

        private List<clsOntologyItem> itemList = new List<clsOntologyItem>();
        private List<clsOntologyItem> preAppliedItems = new List<clsOntologyItem>();

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public OntoListController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            PropertyChanged += OntoListController_PropertyChanged;
        }

        private void OntoListController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);

            
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;


        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
            
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.OItemList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.RemoveOItem,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
            

            itemFileName = Guid.NewGuid().ToString() + ".json";
            var dataSessionFile = webSocketServiceAgent.RequestWriteStream(itemFileName);
            if (dataSessionFile == null) return;

            using (dataSessionFile.StreamWriter)
            {
                using (var jsonWriter = new Newtonsoft.Json.JsonTextWriter(dataSessionFile.StreamWriter))
                {
                    jsonWriter.WriteStartArray();
                    jsonWriter.WriteEndArray();
                }
            }
            ColumnConfig_Grid = new OntoWebCore.Models.JqxColumnList
            {
                ColumnList = new List<OntoWebCore.Attributes.JqxColumnAttribute>
                {
                    new OntoWebCore.Attributes.JqxColumnAttribute(OntoWebCore.Attributes.AlignType.Left, OntoWebCore.Attributes.AlignType.Left, OntoWebCore.Attributes.ColumnType.Checkbox)
                        {
                            datafield = "ItemApply",
                            hidden = false,
                            editable = true,
                            width = 50
                        },
                    new OntoWebCore.Attributes.JqxColumnAttribute(OntoWebCore.Attributes.AlignType.Left, OntoWebCore.Attributes.AlignType.Left, OntoWebCore.Attributes.ColumnType.Textbox)
                    {
                        datafield = "Name"
                    }

                }
            };

            JqxDataSource_Grid = new OntoWebCore.Models.JqxDataSource(OntoWebCore.Models.JsonDataType.Json)
            {
                datafields = new List<OntoWebCore.Models.DataField>
                {
                    new OntoWebCore.Models.DataField(OntoWebCore.Models.DataFieldType.String)
                    {
                        name = "GUID"
                    },
                    new OntoWebCore.Models.DataField(OntoWebCore.Models.DataFieldType.String)
                    {
                        name = "Name"
                    }
                },
                id = "GUID",
                url = dataSessionFile.FileUri.AbsoluteUri
            };


            if (IsSuccessful_Login && webSocketServiceAgent.DedicatedSenderArgument != null)
            {
                var interModMessage = new InterServiceMessage
                {
                    ChannelId = Channels.ViewReady,
                    ReceiverId = webSocketServiceAgent.DedicatedSenderArgument.Value
                };

                webSocketServiceAgent.SendInterModMessage(interModMessage);
            }
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;

         
        }

       

      

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == "AppliedObjectRow")
                {
                    ToggleCheckObjectRow(true);
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "UnAppliedObjectRow")
                {
                    ToggleCheckObjectRow(false);

                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "AddtoClipboard")
                {


                    AddtoClipboard();


                }



            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {
            
                });
            }
            else if (e.PropertyName == NotifyChanges.Websocket_ViewArguments)
            {

            }
            else if (e.PropertyName == NotifyChanges.Websocket_ClassArgument)
            {
                var classId = webSocketServiceAgent.ClassArgument.Value;

                if (string.IsNullOrEmpty(classId) || !localConfig.Globals.is_GUID(classId)) return;

                var classItem = serviceAgent_Elastic.GetOItem(classId, localConfig.Globals.Type_Class);

                if (classItem == null) return;


            }



        }

        private void ToggleCheckObjectRow(bool isChecked)
        {

            if (isChecked)
            {
                var clipBoardItem = itemList.FirstOrDefault(itm => itm.GUID == webSocketServiceAgent.Request["Id"].ToString());
                if (clipBoardItem != null)
                {
                    preAppliedItems.Add(clipBoardItem);
                }
            }
            else
            {
                preAppliedItems.RemoveAll(itm => itm.GUID == webSocketServiceAgent.Request["Id"].ToString());
            }
        }

        private void AddtoClipboard()
        {
            if (preAppliedItems != null && preAppliedItems.Any())
            {
                var interServiceMessage = new InterServiceMessage
                {
                    ChannelId = Channels.AddToClipboard,
                    OItems = preAppliedItems
                };

                webSocketServiceAgent.SendInterModMessage(interServiceMessage);
            }
        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;

            if (message.ChannelId == Channels.OItemList)
            {
                if (message.OItems == null) return;

                preAppliedItems.Clear();
                itemList = message.OItems;

                var jsonItems = message.OItems.Select(oItem => new { GUID = oItem.GUID, Name = oItem.Name }).ToList();

                var json = Newtonsoft.Json.JsonConvert.SerializeObject(jsonItems);
                if (string.IsNullOrEmpty(itemFileName))
                {
                    itemFileName = Guid.NewGuid().ToString() + ".json";
                }
                var sessionFile = webSocketServiceAgent.RequestWriteStream(itemFileName);

                if (sessionFile == null) return;

                using (sessionFile.StreamWriter)
                {
                    sessionFile.StreamWriter.WriteLine(json);
                }
                webSocketServiceAgent.SendCommand("refreshGrid");
            }
            else if (message.ChannelId == Channels.RemoveOItem)
            {
                if (message.OItems == null) return;

                message.OItems.ForEach(oItem =>
                {
                    var oItemInList = itemList.FirstOrDefault(item => item.GUID == oItem.GUID);

                    if (oItemInList != null)
                    {
                        itemList.Remove(oItemInList);
                        Text_RowRemove = oItem.GUID;
                    }

                    var preAppliedItem = preAppliedItems.FirstOrDefault(item => item.GUID == oItem.GUID);

                    if (preAppliedItem != null)
                    {
                        preAppliedItems.Remove(preAppliedItem);
                    }
                });
            }
            if (channelsToAdd != null && channelsToAdd.Contains(message.ChannelId))
            {
                if (message.OItems == null) return;

                message.OItems.ForEach(oItem =>
                {
                    if (!itemList.Any(item => item.GUID == oItem.GUID))
                    {
                        itemList.Add(oItem);
                        OItem_RowAdd = oItem;
                    }
                });

            }
            if (channelsToRemove != null && channelsToRemove.Contains(message.ChannelId))
            {
                var channelParam = webSocketServiceAgent.ViewArguments.FirstOrDefault(arg => arg.KeyFound("ChannelsForAdd"));

                if (channelParam != null)
                {
                    
                    var channels = channelParam.Value.Split(',');

                    if (channels.Any())
                    {
                        channelsToAdd = channels.ToList();
                    }
                }

                if (channelsToAdd != null)
                {
                    channelsToAdd.ForEach(channel =>
                    {
                        webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
                        {
                            ChannelTypeId = channel,
                            EndPointId = webSocketServiceAgent.EndpointId,
                            EndpointType = EndpointType.Receiver,
                            SessionId = webSocketServiceAgent.DataText_SessionId
                        });
                    });
                }

                channelParam = webSocketServiceAgent.ViewArguments.FirstOrDefault(arg => arg.KeyFound("ChannelsForRemove"));

                if (channelParam != null)
                {
                    var channels = channelParam.Value.Split(',');

                    if (channels.Any())
                    {
                        channelsToRemove = channels.ToList();
                    }
                }

                if (channelsToRemove != null)
                {
                    channelsToRemove.ForEach(channel =>
                    {
                        webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
                        {
                            ChannelTypeId = channel,
                            EndPointId = webSocketServiceAgent.EndpointId,
                            EndpointType = EndpointType.Receiver,
                            SessionId = webSocketServiceAgent.DataText_SessionId
                        });
                    });
                }
            }
        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
