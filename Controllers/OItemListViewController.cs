﻿using OItemListController.Factories;
using OItemListController.Models;
using OItemListController.Services;
using OItemListController.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.Services;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Attributes;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.ComponentModel;
using OItemListController.Statemachines;

namespace OItemListController.Controllers
{
    public class OItemListViewController : OItemListViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private ViewModelOItemList oitemListViewModel1;
        private ViewModelOItemList oitemListViewModel2;

        private DataGridJsonFactory<InstanceViewItem> objectFactory = new DataGridJsonFactory<InstanceViewItem>();
        private DataGridJsonFactory<AttributeTypeViewItem> attributeTypeFactory = new DataGridJsonFactory<AttributeTypeViewItem>();
        private DataGridJsonFactory<RelationTypeViewItem> relationTypeFactory = new DataGridJsonFactory<RelationTypeViewItem>();
        private DataGridJsonFactory<ObjectObject_Conscious> objectRelLeftRightFactory = new DataGridJsonFactory<ObjectObject_Conscious>();
        private DataGridJsonFactory<ObjectObject_Subconscious> objectRelRightLeftFactory = new DataGridJsonFactory<ObjectObject_Subconscious>();
        private DataGridJsonFactory<ObjectObject_Omni> objectRelOmniFactory = new DataGridJsonFactory<ObjectObject_Omni>();
        private DataGridJsonFactory<ObjectAttributeViewItem> objectAttributeFactory = new DataGridJsonFactory<ObjectAttributeViewItem>();

        private TranslationController translationController = new TranslationController();

        private ServiceAgent_ElasticOItemList serviceAgent_Elastic;

        private clsOntologyItem oItem;
        private clsOntologyItem oItemClass;
        private List<ObjectObject_Omni> omniRelationItems = new List<ObjectObject_Omni>();
        private ViewModelPropertyCollection sendControlProperties;

        private clsLocalConfig localConfig;

        private SessionFile dataSessionFile;

        private Timer timerSelection;
        private Timer timerNameChange;

        private ListType listType;

        private List<string> selectedNodeIds = new List<string>();
        private List<string> appliedNodeIds = new List<string>();

        private List<AppliedItem> appliedItems = new List<AppliedItem>();

        private ClassAttributeNode classAttributeNode;
        public ObjectRelNode objectRelNode;

        private AttributeTypeViewItem attributeTypeChangeItem;
        private RelationTypeViewItem relationTypeChangeItem;
        private InstanceViewItem instanceChangeItem;

        private clsOntologyItem selectedItem;


        private bool editInProgress;

        private string fileNameData;

        public IControllerStateMachine StateMachine { get; private set; }
        public OItemListStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (OItemListStateMachine)StateMachine;
            }
        }

        public OItemListViewController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            PropertyChanged += OItemListViewController_PropertyChanged;

        }

        private void OItemListViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            
            if (!sendControlProperties.ContainsViewModelProperty(e.PropertyName) || sendControlProperties.IsMarked(e.PropertyName))
            {
                webSocketServiceAgent.SendPropertyChange(e.PropertyName);
            }
            if (e.PropertyName == Notifications.NotifyChanges.ViewModel_CellItem_CellSelected)
            {
                if (CellItem_CellSelected != null)
                {
                    if (CellItem_CellSelected.ColumnName == "NameInstance" ||
                        CellItem_CellSelected.ColumnName == "NameRelationType" ||
                        CellItem_CellSelected.ColumnName == "NameAttributeType" ||
                        CellItem_CellSelected.ColumnName == "NameObject" ||
                        CellItem_CellSelected.ColumnName == "NameOther" ||
                        CellItem_CellSelected.ColumnName == "NameParentObject" ||
                        CellItem_CellSelected.ColumnName == "NameParentOther")
                    {

                        var item = GetRelationNodeById(CellItem_CellSelected.Id);

                        if (item is ObjectObject_Conscious ||
                            item is ObjectObject_Omni)
                        {

                            var relItem = (ObjectObject_Conscious)item;
                            if (CellItem_CellSelected.ColumnName == "NameOther")
                            {
                                var idObject = relItem.IdOther;
                                Text_CellGuid = idObject;
                                selectedItem = serviceAgent_Elastic.GetOItem(idObject, localConfig.Globals.Type_Object);
                            }
                            else if (CellItem_CellSelected.ColumnName == "NameParentOther")
                            {
                                var idClass = relItem.IdParentOther;
                                selectedItem = serviceAgent_Elastic.GetOItem(idClass, localConfig.Globals.Type_Class);
                            }

                        }
                        else if (item is ObjectObject_Omni)
                        {
                            var relItem = (ObjectObject_Omni)item;
                            if (CellItem_CellSelected.ColumnName == "NameObject")
                            {
                                var idObject = relItem.IdObject;
                                Text_CellGuid = idObject;
                                selectedItem = serviceAgent_Elastic.GetOItem(idObject, localConfig.Globals.Type_Object);
                            }
                            else if (CellItem_CellSelected.ColumnName == "NameOther")
                            {
                                var idObject = relItem.IdOther;
                                Text_CellGuid = idObject;
                                selectedItem = serviceAgent_Elastic.GetOItem(idObject, localConfig.Globals.Type_Object);
                            }
                            else if (CellItem_CellSelected.ColumnName == "NameParentObject")
                            {
                                var idClass = relItem.IdParentObject;
                                selectedItem = serviceAgent_Elastic.GetOItem(idClass, localConfig.Globals.Type_Class);
                            }
                            else if (CellItem_CellSelected.ColumnName == "NameParentOther")
                            {
                                var idClass = relItem.IdParentOther;
                                selectedItem = serviceAgent_Elastic.GetOItem(idClass, localConfig.Globals.Type_Class);
                            }
                        }
                        else if (item is ObjectObject_Subconscious)
                        {
                            var relItem = (ObjectObject_Subconscious)item;
                            if (CellItem_CellSelected.ColumnName == "NameObject")
                            {
                                var idObject = relItem.IdObject;
                                Text_CellGuid = idObject;
                                selectedItem = serviceAgent_Elastic.GetOItem(idObject, localConfig.Globals.Type_Object);
                            }
                            else if (CellItem_CellSelected.ColumnName == "NameParentOther")
                            {
                                var idClass = relItem.IdParentOther;
                                selectedItem = serviceAgent_Elastic.GetOItem(idClass, localConfig.Globals.Type_Class);
                            }
                        }
                        else if (item is InstanceViewItem)
                        {
                            var instanceItem = (InstanceViewItem)item;
                            Text_CellGuid = instanceItem.IdInstance;
                            selectedItem = new clsOntologyItem
                            {
                                GUID = instanceItem.IdInstance,
                                Name = instanceItem.NameInstance,
                                GUID_Parent = instanceItem.IdClass,
                                Type = localConfig.Globals.Type_Object
                            };

                        }
                        else if (item is RelationTypeViewItem)
                        {
                            var relationTypeItem = (RelationTypeViewItem)item;
                            Text_CellGuid = relationTypeItem.IdRelationType;
                            selectedItem = new clsOntologyItem
                            {
                                GUID = relationTypeItem.IdRelationType,
                                Name = relationTypeItem.NameRelationType,
                                Type = localConfig.Globals.Type_RelationType
                            };
                        }

                        if (selectedItem != null && selectedItem.Type == localConfig.Globals.Type_Object)
                        {
                            Text_CellGuid = selectedItem.GUID;
                            var selectedObjectRequest = new InterServiceMessage
                            {
                                ChannelId = Channels.ParameterList,
                                OItems = new List<clsOntologyItem>
                                {
                                    selectedItem
                                }
                            };
                            webSocketServiceAgent.SendInterModMessage(selectedObjectRequest);
                        }
                        else if (selectedItem != null && selectedItem.Type == localConfig.Globals.Type_Class)
                        {
                            var selectClassRequest = new InterServiceMessage
                            {
                                ChannelId = Channels.SelectClassNode,
                                OItems = new List<clsOntologyItem>
                                {
                                    selectedItem
                                }
                            };
                            webSocketServiceAgent.SendInterModMessage(selectClassRequest);
                        }
                        else if (selectedItem != null && selectedItem.Type == localConfig.Globals.Type_RelationType)
                        {
                            var selectedRelationTypeRequest = new InterServiceMessage
                            {
                                ChannelId = Channels.SelectedRelationNode,
                                OItems = new List<clsOntologyItem>
                                {
                                    selectedItem
                                }
                            };

                            webSocketServiceAgent.SendInterModMessage(selectedRelationTypeRequest);
                        }
                    }
                    
                }
                
            }
            else if (e.PropertyName == Notifications.NotifyChanges.ViewModel_CellItem_CellDoubleClicked)
            {
                if (CellItem_CellDoubleClicked != null)
                {
                    if (CellItem_CellDoubleClicked.ColumnName == "NameInstance" ||
                        CellItem_CellDoubleClicked.ColumnName == "NameRelationType" ||
                        CellItem_CellDoubleClicked.ColumnName == "NameAttributeType")
                    {
                        if (listType == ListType.AttributeTypes)
                        {
                            attributeTypeChangeItem = oitemListViewModel1.ListAdapter.AttributeTypeViewItems.FirstOrDefault(attType => attType.IdItem == CellItem_CellDoubleClicked.Id);
                            IsEnabled_SaveName = false;
                            Text_Guid = attributeTypeChangeItem.IdAttributeType;
                            SetNameValue(attributeTypeChangeItem.NameAttributeType);
                            webSocketServiceAgent.SendCommand("OpenEditName");
                        }
                        else if (listType == ListType.RelationTypes)
                        {
                            relationTypeChangeItem = oitemListViewModel1.ListAdapter.RelationTypeViewItems.FirstOrDefault(relType => relType.IdItem == CellItem_CellDoubleClicked.Id);
                            IsEnabled_SaveName = false;
                            Text_Guid = relationTypeChangeItem.IdRelationType;
                            SetNameValue(relationTypeChangeItem.NameRelationType);
                            
                            webSocketServiceAgent.SendCommand("OpenEditName");

                        }
                        else if (listType == ListType.Instances)
                        {
                            instanceChangeItem = oitemListViewModel1.ListAdapter.InstanceViewItems.FirstOrDefault(instance => instance.IdItem == CellItem_CellDoubleClicked.Id);
                            IsEnabled_SaveName = false;
                            Text_Guid = instanceChangeItem.IdInstance;
                            SetNameValue(instanceChangeItem.NameInstance);
                            webSocketServiceAgent.SendCommand("OpenEditName");
                        }
                    }
                    else if (CellItem_CellDoubleClicked.ColumnName == "OrderId")
                    {
                        
                        if (listType == ListType.InstanceInstance_Conscious)
                        {
                            var leftRightItem = oitemListViewModel1.ListAdapter.ObjectObjectConsciousViewItems.FirstOrDefault(instance => instance.IdItem == CellItem_CellDoubleClicked.Id);

                            Int_OrderIdValue = (int)leftRightItem.OrderId;
                            webSocketServiceAgent.SendCommand("OpenEditOrderId");
                        }
                        else if (listType == ListType.InstanceInstance_Subconscious)
                        {
                            var rightLeftItem = oitemListViewModel1.ListAdapter.ObjectObjectSubconsciousViewItems.FirstOrDefault(instance => instance.IdItem == CellItem_CellDoubleClicked.Id);
                            Int_OrderIdValue = (int)rightLeftItem.OrderId;
                            webSocketServiceAgent.SendCommand("OpenEditOrderId");

                        }
                        else if (listType == ListType.InstanceOther_Conscious)
                        {
                            var relItem = oitemListViewModel1.ListAdapter.ObjectOtherConsciousViewItems.FirstOrDefault(instance => instance.IdItem == CellItem_CellDoubleClicked.Id);
                            Int_OrderIdValue = (int)relItem.OrderId;
                            webSocketServiceAgent.SendCommand("OpenEditOrderId");

                        }
                        else if (listType == ListType.InstanceOther_Subconscious)
                        {
                            var relItem = oitemListViewModel1.ListAdapter.ObjectOtherSubconsciousViewItems.FirstOrDefault(instance => instance.IdItem == CellItem_CellDoubleClicked.Id);
                            Int_OrderIdValue = (int)relItem.OrderId;
                            webSocketServiceAgent.SendCommand("OpenEditOrderId");
                        }
                        else if (listType == ListType.InstanceAttribute_Conscious)
                        {
                            var relItem = oitemListViewModel1.ListAdapter.ObjectAttributeViewItems.FirstOrDefault(instance => instance.IdItem == CellItem_CellDoubleClicked.Id);
                            Int_OrderIdValue = (int)relItem.OrderId;
                            webSocketServiceAgent.SendCommand("OpenEditOrderId");
                        }
                        
                        
                    }
                }
                
            }
            
        }

        private void SetNameValue(string nameValue)
        {
            sendControlProperties.SetMark(Notifications.NotifyChanges.ViewModel_DataText_NameValue, true);
            DataText_NameValue = nameValue;
            sendControlProperties.SetMark(Notifications.NotifyChanges.ViewModel_DataText_NameValue, false);
        }
        private void ParamItemCheck()
        {
            var objectArg = webSocketServiceAgent.ViewArguments.FirstOrDefault(arg => arg.KeyFound("Object"));
            var relationTypeArg = webSocketServiceAgent.ViewArguments.FirstOrDefault(arg => arg.KeyFound("RelationType"));
            var idLeftArg = webSocketServiceAgent.ViewArguments.FirstOrDefault(arg => arg.KeyFound("IdLeft"));
            var idRightArg = webSocketServiceAgent.ViewArguments.FirstOrDefault(arg => arg.KeyFound("IdRight"));
            var idRelationTypeArt = webSocketServiceAgent.ViewArguments.FirstOrDefault(arg => arg.KeyFound("IdRelationType"));

            var idObject = objectArg != null ? objectArg.Value: null;
            var relationType = relationTypeArg != null ? relationTypeArg.Value : null;
            var idLeft = idLeftArg != null ? idLeftArg.Value : null;
            var idRight = idRightArg != null ? idRightArg.Value : null;
            var idRelationType = idRelationTypeArt != null ? idRelationTypeArt.Value : null;

            if (!string.IsNullOrEmpty(relationType))
            {

                clsOntologyItem objectItem = null;
                ObjectRelNode relNode = null;

                if (relationType == ListType.InstanceInstance_Conscious.ToString())
                {
                    objectItem = serviceAgent_Elastic.GetOItem(idLeft, localConfig.Globals.Type_Object);

                    relNode = new ObjectRelNode(true, false);
                    relNode.IdLeft = objectItem.GUID_Parent;
                    relNode.IdRight = idRight;
                    relNode.NodeType = NodeType.NodeForwardFormal;

                    listType = ListType.InstanceAttribute_Conscious;

                }
                else if (relationType == ListType.InstanceInstance_Subconscious.ToString())
                {
                    objectItem = serviceAgent_Elastic.GetOItem(idRight, localConfig.Globals.Type_Object);

                    relNode = new ObjectRelNode(true, false);
                    relNode.IdLeft = idLeft;
                    relNode.IdRight = objectItem.GUID_Parent;
                    relNode.NodeType = NodeType.NodeBackwardFormal;

                    listType = ListType.InstanceInstance_Subconscious;


                }

                if (objectItem == null) return;

                oItem = objectItem;




                relNode.IdRelationType = idRelationType;

                objectRelNode = relNode;
                BeginLoadItems();
                return;
            }
            else if (idObject != null)
            {
                clsOntologyItem objectItem = null;
                ObjectRelNode relNode = null;

                if (listType == ListType.InstanceInstance_Conscious)
                {
                    objectItem = serviceAgent_Elastic.GetOItem(idObject, localConfig.Globals.Type_Object);

                    relNode = new ObjectRelNode(true, false);
                    relNode.IdLeft = objectItem.GUID;
                    relNode.IdRight = idRight;
                    relNode.NodeType = NodeType.NodeForwardFormal;


                }
                else if (listType == ListType.InstanceInstance_Subconscious)
                {
                    objectItem = serviceAgent_Elastic.GetOItem(idObject, localConfig.Globals.Type_Object);

                    relNode = new ObjectRelNode(true, false);
                    relNode.IdLeft = idLeft;
                    relNode.IdRight = objectItem.GUID;
                    relNode.NodeType = NodeType.NodeBackwardFormal;



                }

                if (objectItem == null) return;

                oItem = objectItem;


                if (idRelationType != null)
                {
                    relNode.IdRelationType = idRelationType;
                }

                objectRelNode = relNode;
                BeginLoadItems();
                return;
            }
            
            AdvancedFilterCheck();
        }

        private void AdvancedFilterCheck()
        {
            var rootObjectItem = webSocketServiceAgent.ViewArguments.FirstOrDefault(arg => arg.KeyFound("Object"));
            var rootClassItem = webSocketServiceAgent.ViewArguments.FirstOrDefault(arg => arg.KeyFound("Class"));
            var classFilterParam = webSocketServiceAgent.ViewArguments.FirstOrDefault(arg => arg.KeyFound("FilterClass"));
            var objectFilterParam = webSocketServiceAgent.ViewArguments.FirstOrDefault(arg => arg.KeyFound("FilterObject"));
            var relationTypeFilterParam = webSocketServiceAgent.ViewArguments.FirstOrDefault(arg => arg.KeyFound("FilterRelationType"));
            var directionFilterParam = webSocketServiceAgent.ViewArguments.FirstOrDefault(arg => arg.KeyFound("FilterDirection"));

            var viewId = "";
            if (rootObjectItem == null &&
                rootClassItem == null) return;

            clsOntologyItem oItemRoot = null;
            if (rootObjectItem != null)
            {
                var rootObjectId = rootObjectItem.Value;
                if (!localConfig.Globals.is_GUID(rootObjectId)) return;

                oItemRoot = serviceAgent_Elastic.GetOItem(rootObjectId, localConfig.Globals.Type_Object);

                DataText_Filter = oItemRoot.GUID;
            }
            else if (rootClassItem != null)
            {
                var rootClassId = rootClassItem.Value;
                if (!localConfig.Globals.is_GUID(rootClassItem.Value)) return;

                oItemRoot = serviceAgent_Elastic.GetOItem(rootClassId, localConfig.Globals.Type_Class);
            }

            if (oItemRoot == null) return;


            if (oItemRoot.Type == localConfig.Globals.Type_Object)
            {
                var oItemRootClass = serviceAgent_Elastic.GetOItem(oItemRoot.GUID_Parent, localConfig.Globals.Type_Class);

                Text_View = oItemRootClass.Name + ": " + oItemRoot.Name;
            }
            else
            {
                Text_View = oItemRoot.Name;
            }


            if (classFilterParam == null &&
                objectFilterParam == null) return;

            clsOntologyItem oItemObject = null;
            clsOntologyItem oItemClass = null;
            if (objectFilterParam != null)
            {
                var objectId = objectFilterParam.Value;

                if (!string.IsNullOrEmpty(objectId))
                {
                    oItemObject = serviceAgent_Elastic.GetOItem(objectId, localConfig.Globals.Type_Object);
                    if (oItemObject == null) return;
                }
            }
            else
            {
                var classId = classFilterParam.Value;

                oItemClass = serviceAgent_Elastic.GetOItem(classId, localConfig.Globals.Type_Class);

                if (oItemClass == null) return;
            }


            if (relationTypeFilterParam == null ||
                    directionFilterParam == null)
            {
                return;
            }



            var relationTypeId = relationTypeFilterParam.Value;
            var directionId = directionFilterParam.Value;

            if (!localConfig.Globals.is_GUID(relationTypeId) ||
                !localConfig.Globals.is_GUID(directionId)) return;


            var oItemRelationType = serviceAgent_Elastic.GetOItem(relationTypeId, localConfig.Globals.Type_RelationType);
            clsOntologyItem oItemDirection = null;

            if (directionId == localConfig.Globals.Direction_LeftRight.GUID)
            {
                oItemDirection = localConfig.Globals.Direction_LeftRight;
            }
            else if (directionId == localConfig.Globals.Direction_RightLeft.GUID)
            {
                oItemDirection = localConfig.Globals.Direction_RightLeft;
            }



            if (oItemRelationType == null ||
                oItemDirection == null) return;

            IsVisible_Apply = true;


            oitemListViewModel1.Initialize_AdvancedFilter(ListType.Instances, oItemRoot, oItemClass, oItemObject, oItemRelationType, oItemDirection);
        }

        private object GetRelationNodeById(string nodeId)
        {
            if (listType == ListType.AttributeTypes)
            {
                var attributeType = oitemListViewModel1.ListAdapter.AttributeTypeViewItems.FirstOrDefault(attType => attType.IdItem == nodeId);

                return attributeType;
            }
            else if (listType == ListType.RelationTypes)
            {
                var relationtypeType = oitemListViewModel1.ListAdapter.RelationTypeViewItems.FirstOrDefault(relType => relType.IdItem == nodeId);

                return relationtypeType;
            }
            else if (listType == ListType.Instances)
            {
                var instanceItem = oitemListViewModel1.ListAdapter.InstanceViewItems.FirstOrDefault(instance => instance.IdItem == nodeId);
                return instanceItem;
            }
            else if (listType == ListType.InstanceInstance_Conscious)
            {
                var leftRightItem = oitemListViewModel1.ListAdapter.ObjectObjectConsciousViewItems.FirstOrDefault(instance => instance.IdItem == nodeId);
                return leftRightItem;

            }
            else if (listType == ListType.InstanceInstance_Subconscious)
            {
                var rightLeftItem = oitemListViewModel1.ListAdapter.ObjectObjectSubconsciousViewItems.FirstOrDefault(instance => instance.IdItem == nodeId);
                return rightLeftItem;

            }
            else if (listType == ListType.InstanceOther_Conscious)
            {
                var relItem = oitemListViewModel1.ListAdapter.ObjectOtherConsciousViewItems.FirstOrDefault(instance => instance.IdItem == nodeId);
                return relItem;

            }
            else if (listType == ListType.InstanceOther_Subconscious)
            {
                var relItem = oitemListViewModel1.ListAdapter.ObjectOtherSubconsciousViewItems.FirstOrDefault(instance => instance.IdItem == nodeId);
                return relItem;
            }
            else if (listType == ListType.InstanceAttribute_Conscious)
            {
                var relItem = oitemListViewModel1.ListAdapter.ObjectAttributeViewItems.FirstOrDefault(instance => instance.IdItem == nodeId);
                return relItem;
            }

            return null;
        }
        
        private void Initialize()
        {
            var stateMachine = new OItemListStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            oitemListViewModel1 = new ViewModelOItemList(localConfig.Globals);
            oitemListViewModel1.PropertyChanged += ViewModelWinFormsOItemList_PropertyChanged;
            oitemListViewModel2 = new ViewModelOItemList(localConfig.Globals);
            oitemListViewModel2.PropertyChanged += OitemListViewModel2_PropertyChanged;

            timerSelection = new Timer();
            timerSelection.Interval = 300;
            timerSelection.Elapsed += TimerSelection_Elapsed;

            timerNameChange = new Timer();
            timerNameChange.Interval = 500;
            timerNameChange.Elapsed += TimerNameChange_Elapsed;

            serviceAgent_Elastic = new ServiceAgent_ElasticOItemList(localConfig);
            serviceAgent_Elastic.PropertyChanged += ServiceAgent_Elastic_PropertyChanged;

            sendControlProperties = new ViewModelPropertyCollection(this, SpecialCollectionType.AvoidSend);
            sendControlProperties.AddProperty(Notifications.NotifyChanges.ViewModel_DataText_NameValue);
        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();


            oitemListViewModel1 = null;
            objectFactory = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            //if (webSocketServiceAgent.Request.ContainsKey("ListType"))
            //{

            //}

            var viewItem = ModuleDataExchanger.GetViewById(webSocketServiceAgent.Context.QueryString["View"]);
            if (viewItem == null) return;

            if (viewItem.IdView == "f41336728e3a4c8382049d7b4edf7b80")
            {
                listType = ListType.Instances;
            }
            else if (viewItem.IdView == "45277c877f12425b901683073ab859bb")
            {
                listType = ListType.AttributeTypes;
            }
            else if (viewItem.IdView == "ebefefb90b304f5d9e563456ab3a4810")
            {
                listType = ListType.RelationTypes;
            }
            else if (viewItem.IdView == "e7542106ac9a41bc95b826d826cec5b0")
            {
                listType = ListType.InstanceInstance_Conscious;
            }
            else if (viewItem.IdView == "3579ff1dc8b047939531c0299f982394")
            {
                listType = ListType.InstanceInstance_Subconscious;
            }
            else if (viewItem.IdView == "44dd044245574fb2bc82aed00e9fba7a")
            {
                listType = ListType.InstanceInstance_Omni;
            }
            else if (viewItem.IdView == "cb405518af064996812c45f51d13b6e2")
            {
                listType = ListType.InstanceOther_Conscious;
            }
            else if (viewItem.IdView == "8f1da317a4f34b608cee0eb750f0a773")
            {
                listType = ListType.InstanceOther_Subconscious;
            }
            else if (viewItem.IdView == "6009ef41b9be459b9c2b62e89e5c155d")
            {
                listType = ListType.InstanceAttribute_Conscious;
            }
        }

        private void StateMachine_loginSucceded()
        {

            



            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.SelectedClassNode,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.AddedObjects,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.SelectedRelationNode,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ApplyFilter,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            Label_CopyGuid = translationController.Label_Guid;
            Label_SaveName = translationController.Label_Save;
            Label_SaveOrderId = translationController.Label_Save;
            Label_ValueName = translationController.Label_Name;
            Label_ValueOrderId = translationController.Label_OrderId;

            IsEnabled_SaveName = false;
            IsEnabled_SaveOrderId = false;
            IsEnabled_AddToClipboard = false;
            IsEnabled_Apply = false;

            IsToggled_Listen = true;





            if (string.IsNullOrEmpty(fileNameData))
            {
                fileNameData = Guid.NewGuid().ToString() + ".json";
            }

            dataSessionFile = webSocketServiceAgent.RequestWriteStream(fileNameData);
            if (dataSessionFile == null) return;

            using (dataSessionFile.StreamWriter)
            {
                using (var jsonWriter = new Newtonsoft.Json.JsonTextWriter(dataSessionFile.StreamWriter))
                {
                    jsonWriter.WriteStartArray();
                    jsonWriter.WriteEndArray();
                }
            }

            System.Threading.Thread.Sleep(500);
            
            List<ListTypePropertAttribute> propertyAttributes = null;
            if (listType == ListType.Instances)
            {
                propertyAttributes = typeof(InstanceViewItem).GetProperties().Cast<PropertyInfo>().Select(prop =>
                {
                    var attributeItem = (DataViewColumnAttribute)prop.GetCustomAttribute(typeof(DataViewColumnAttribute));

                    if (attributeItem == null) return null;

                    return new ListTypePropertAttribute { PopertyItem = prop, ColumnAttribute = attributeItem };

                }).Where(propAtt => propAtt != null).ToList();

            }
            else if (listType == ListType.AttributeTypes)
            {
                propertyAttributes = typeof(AttributeTypeViewItem).GetProperties().Cast<PropertyInfo>().Select(prop =>
                {
                    var attributeItem = (DataViewColumnAttribute)prop.GetCustomAttribute(typeof(DataViewColumnAttribute));

                    if (attributeItem == null) return null;

                    return new ListTypePropertAttribute { PopertyItem = prop, ColumnAttribute = attributeItem };

                }).Where(propAtt => propAtt != null).ToList();
            }
            else if (listType == ListType.RelationTypes)
            {
                propertyAttributes = typeof(RelationTypeViewItem).GetProperties().Cast<PropertyInfo>().Select(prop =>
                {
                    var attributeItem = (DataViewColumnAttribute)prop.GetCustomAttribute(typeof(DataViewColumnAttribute));

                    if (attributeItem == null) return null;

                    return new ListTypePropertAttribute { PopertyItem = prop, ColumnAttribute = attributeItem };

                }).Where(propAtt => propAtt != null).ToList();
            }
            else if (listType == ListType.InstanceInstance_Conscious)
            {
                propertyAttributes = typeof(ObjectObject_Conscious).GetProperties().Cast<PropertyInfo>().Select(prop =>
                {
                    var attributeItem = (DataViewColumnAttribute)prop.GetCustomAttribute(typeof(DataViewColumnAttribute));

                    if (attributeItem == null) return null;

                    return new ListTypePropertAttribute { PopertyItem = prop, ColumnAttribute = attributeItem };

                }).Where(propAtt => propAtt != null).ToList();
            }
            else if (listType == ListType.InstanceInstance_Subconscious)
            {
                propertyAttributes = typeof(ObjectObject_Subconscious).GetProperties().Cast<PropertyInfo>().Select(prop =>
                {
                    var attributeItem = (DataViewColumnAttribute)prop.GetCustomAttribute(typeof(DataViewColumnAttribute));

                    if (attributeItem == null) return null;

                    return new ListTypePropertAttribute { PopertyItem = prop, ColumnAttribute = attributeItem };

                }).Where(propAtt => propAtt != null).ToList();
            }
            else if (listType == ListType.InstanceInstance_Omni)
            {
                propertyAttributes = typeof(ObjectObject_Omni).GetProperties().Cast<PropertyInfo>().Select(prop =>
                {
                    var attributeItem = (DataViewColumnAttribute)prop.GetCustomAttribute(typeof(DataViewColumnAttribute));

                    if (attributeItem == null) return null;

                    return new ListTypePropertAttribute { PopertyItem = prop, ColumnAttribute = attributeItem };

                }).Where(propAtt => propAtt != null).ToList();
            }
            else if (listType == ListType.InstanceOther_Conscious)
            {
                propertyAttributes = typeof(ObjectObject_Conscious).GetProperties().Cast<PropertyInfo>().Select(prop =>
                {
                    var attributeItem = (DataViewColumnAttribute)prop.GetCustomAttribute(typeof(DataViewColumnAttribute));

                    if (attributeItem == null) return null;

                    return new ListTypePropertAttribute { PopertyItem = prop, ColumnAttribute = attributeItem };

                }).Where(propAtt => propAtt != null).ToList();
            }
            else if (listType == ListType.InstanceOther_Subconscious)
            {
                propertyAttributes = typeof(ObjectObject_Subconscious).GetProperties().Cast<PropertyInfo>().Select(prop =>
                {
                    var attributeItem = (DataViewColumnAttribute)prop.GetCustomAttribute(typeof(DataViewColumnAttribute));

                    if (attributeItem == null) return null;

                    return new ListTypePropertAttribute { PopertyItem = prop, ColumnAttribute = attributeItem };

                }).Where(propAtt => propAtt != null).ToList();
            }
            else if (listType == ListType.InstanceAttribute_Conscious)
            {
                propertyAttributes = typeof(ObjectAttributeViewItem).GetProperties().Cast<PropertyInfo>().Select(prop =>
                {
                    var attributeItem = (DataViewColumnAttribute)prop.GetCustomAttribute(typeof(DataViewColumnAttribute));

                    if (attributeItem == null) return null;

                    return new ListTypePropertAttribute { PopertyItem = prop, ColumnAttribute = attributeItem };

                }).Where(propAtt => propAtt != null).ToList();
            }

            if (propertyAttributes != null)
            {
                var dataFieldsColumns = propertyAttributes.Select(propAtt =>
                {
                    DataField dataField = null;
                    JqxColumnAttribute column = null;
                    if (propAtt.ColumnAttribute.CellType == CellType.Boolean)
                    {
                        dataField = new DataField(DataFieldType.Bool);
                        column = new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.Checkbox);
                    }
                    else if (propAtt.ColumnAttribute.CellType == CellType.Date)
                    {
                        dataField = new DataField(DataFieldType.Date);
                        column = new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.Textbox);
                    }
                    else if (propAtt.ColumnAttribute.CellType == CellType.Integer)
                    {
                        dataField = new DataField(DataFieldType.Int);
                        column = new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.Textbox);
                    }
                    else if (propAtt.ColumnAttribute.CellType == CellType.Number)
                    {
                        dataField = new DataField(DataFieldType.Number);
                        column = new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.Textbox);
                    }
                    else
                    {
                        dataField = new DataField(DataFieldType.String);
                        column = new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.Textbox);
                    }

                    column.editable = false;
                    column.datafield = propAtt.PopertyItem.Name;
                    column.width = propAtt.ColumnAttribute.Width;
                    if (!propAtt.ColumnAttribute.IsVisible)
                    {

                        column.hidden = true;

                    }

                    dataField.name = propAtt.PopertyItem.Name;

                    return new { DataField = dataField, ColumnConfig = column, AttributeItem = propAtt.ColumnAttribute };

                }).ToList();

                var jqxDataSource_Grid = new JqxDataSource(JsonDataType.Json)
                {
                    datafields = dataFieldsColumns.Select(dataFieldColumn => dataFieldColumn.DataField).ToList(),
                    pagenum = 0,
                    pagesize = 20,
                    url = dataSessionFile.FileUri.AbsoluteUri
                };

                if (IsVisible_Apply)
                {
                    jqxDataSource_Grid.datafields.Add(new DataField(DataFieldType.Bool)
                    {
                        name = "ItemApply"
                    });
                }

                JqxDataSource_Grid = jqxDataSource_Grid;



                var columnConfig_Grid = new JqxColumnList
                {
                    ColumnList = dataFieldsColumns.OrderBy(dataFieldColumn => dataFieldColumn.AttributeItem.DisplayOrder).Select(dataFieldColumn => dataFieldColumn.ColumnConfig).ToList()
                };

                if (IsVisible_Apply)
                {
                    columnConfig_Grid.ColumnList.Insert(0, new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.Checkbox)
                    {
                        datafield = "ItemApply",
                        hidden = false,
                        editable = true,
                        width = 50
                    });
                }

                ColumnConfig_Grid = columnConfig_Grid;

                var parameterRequest = new InterServiceMessage
                {
                    ChannelId = Channels.ParameterList,
                    SenderId = webSocketServiceAgent.EndpointId
                };
                webSocketServiceAgent.SendInterModMessage(parameterRequest);


                if (!string.IsNullOrEmpty(DataText_ClassId))
                {
                    ReceivedItems();

                }
                else if (!string.IsNullOrEmpty(DataText_ObjectId))
                {
                    ReceivedItems();
                }
                else
                {
                    LocalStateMachine.ItemListLoaded();
                }

            }

            if (listType == ListType.AttributeTypes)
            {
                clsOntologyItem filterItem = null;
                LocalStateMachine.ItemListLoadRequest();
                oitemListViewModel1.Initialize_ItemList(ListType.AttributeTypes, filterItem);
            }
            else if (listType == ListType.RelationTypes)
            {
                clsOntologyItem filterItem = null;
                LocalStateMachine.ItemListLoadRequest();
                oitemListViewModel1.Initialize_ItemList(ListType.RelationTypes, filterItem);
            }

            var viewReadyMessage = new InterServiceMessage
            {
                ChannelId = Channels.ViewReady
            };

            webSocketServiceAgent.SendInterModMessage(viewReadyMessage);

            
        }


        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
            else if (e.PropertyName == nameof(OItemListStateMachine.ViewState))
            {
                if (LocalStateMachine.ShowLoader)
                {
                    webSocketServiceAgent.SendCommand(Commands.OpenLoader);
                }
                else
                {
                    webSocketServiceAgent.SendCommand(Commands.CloseLoader);

                }
            }
        }

        private void ServiceAgent_Elastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ServiceAgent_ElasticOItemList.ResultDelete_Items))
            {
                if (serviceAgent_Elastic.ResultDelete_Items.GUID != localConfig.Globals.LState_Error.GUID)
                {
                    appliedItems.ForEach(appliedItem =>
                    {
                        if (listType == ListType.AttributeTypes)
                        {


                        }
                        else if (listType == ListType.RelationTypes)
                        {

                        }
                        else if (listType == ListType.Instances)
                        {

                        }
                        else if (listType == ListType.InstanceInstance_Conscious)
                        {

                            Text_RowRemove = ((ObjectObject_Conscious)appliedItem.ListItem).IdItem;

                        }
                        else if (listType == ListType.InstanceInstance_Omni)
                        {
                            Text_RowRemove = ((ObjectObject_Omni)appliedItem.ListItem).IdItem;
                        }
                        else if (listType == ListType.InstanceInstance_Subconscious)
                        {

                            Text_RowRemove = ((ObjectObject_Subconscious)appliedItem.ListItem).IdItem;
                            
                        }
                        else if (listType == ListType.InstanceOther_Conscious)
                        {

                            Text_RowRemove = ((ObjectObject_Conscious)appliedItem.ListItem).IdItem;
                        }
                        else if (listType == ListType.InstanceOther_Subconscious)
                        {
                            Text_RowRemove = ((ObjectObject_Subconscious)appliedItem.ListItem).IdItem;
                        }
                    });
                }
            }
        }

        private void TimerNameChange_Elapsed(object sender, ElapsedEventArgs e)
        {
            timerNameChange.Stop();

            if (listType == ListType.AttributeTypes)
            {
                var isExisting = serviceAgent_Elastic.IsExisting(DataText_NameValue, true);

                if (isExisting.GUID == localConfig.Globals.LState_Exists.GUID)
                {
                    if (isExisting.OList_Rel.First().GUID == attributeTypeChangeItem.IdAttributeType)
                    {
                        Text_EditMessageName = "";
                        IsEnabled_SaveName = true;
                    }
                    else
                    {
                        Text_EditMessageName = translationController.Text_AttributeTypeExists;
                    }
                    
                }
                else if (isExisting.GUID == localConfig.Globals.LState_Nothing.GUID)
                {
                    Text_EditMessageName = "";
                    IsEnabled_SaveName = true;
                }
            }
            else if (listType == ListType.RelationTypes)
            {
                var isExisting = serviceAgent_Elastic.IsExisting(DataText_NameValue, false);

                if (isExisting.GUID == localConfig.Globals.LState_Exists.GUID)
                {
                    if (isExisting.OList_Rel.First().GUID == relationTypeChangeItem.IdRelationType)
                    {
                        Text_EditMessageName = "";
                        IsEnabled_SaveName = true;
                    }
                    else
                    {
                        Text_EditMessageName = translationController.Text_RelationTypeExists;
                    }
                    
                }
                else if (isExisting.GUID == localConfig.Globals.LState_Nothing.GUID)
                {
                    Text_EditMessageName = "";
                    IsEnabled_SaveName = true;
                }

            }
            else if (listType == ListType.Instances)
            {
                var isExisting = serviceAgent_Elastic.IsExisting(DataText_NameValue, instanceChangeItem.IdClass);

                if (isExisting.GUID == localConfig.Globals.LState_Exists.GUID)
                {
                    if (isExisting.OList_Rel.First().GUID == instanceChangeItem.IdInstance)
                    {
                        Text_EditMessageName = "";
                        IsEnabled_SaveName = true;
                    }
                    else
                    {
                        Text_EditMessageName = translationController.Text_InstanceExists;
                        IsEnabled_SaveName = true;
                    }
                    
                }
                else if (isExisting.GUID == localConfig.Globals.LState_Nothing.GUID)
                {
                    Text_EditMessageName = "";
                    IsEnabled_SaveName = true;
                }
            }
        }

        private void TimerRelSelecteion_Elapsed(object sender, ElapsedEventArgs e)
        {
            
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;

            listType = ListType.Instances;
            
            
            
        }

       

        public void ReceivedItems()
        {
            if (!string.IsNullOrEmpty(DataText_ClassId))
            {
                oItem = serviceAgent_Elastic.GetOItem(DataText_ClassId, localConfig.Globals.Type_Class);
                if (oItem != null)
                {
                    IsVisible_Apply = true;
                    LocalStateMachine.ItemListLoadRequest();
                    oitemListViewModel1.Initialize_ItemList(ListType.Instances, new clsOntologyItem { GUID_Parent = oItem.GUID }, true, DestinationType.None);
                }

            }
            else if (listType == ListType.Instances && !string.IsNullOrEmpty(DataText_ObjectId))
            {
                oItem = serviceAgent_Elastic.GetOItem(DataText_ObjectId, localConfig.Globals.Type_Object);
                oItem.GUID_Related = null;
                if (oItem != null)
                {
                    omniRelationItems.Clear();
                    ReceivedObject();
                }
            }
            else if (listType == ListType.InstanceAttribute_Conscious ||
                listType == ListType.InstanceInstance_Conscious ||
                listType == ListType.InstanceInstance_Omni ||
                listType == ListType.InstanceInstance_Subconscious ||
                listType == ListType.InstanceOther_Conscious ||
                listType == ListType.InstanceOther_Subconscious)
            {

            }

        }

       
        private void SelectedObjectRows()
        {
            var instanceIds = webSocketServiceAgent.Request["IdItems"];
            if (instanceIds == null) return;

            selectedNodeIds = instanceIds.ToString().Split(',').ToList();

            timerSelection.Stop();
            timerSelection.Start();
            //var sendItem = webSocketServiceAgent.Request["IdInstances"].ToString();
            //if (string.IsNullOrEmpty(sendItem)) return;

            //if (!NodeIds_Selected.Contains(sendItem))
            //{
            //    NodeIds_Selected.Add(sendItem);
            //    var oItem = serviceAgent_Elastic.GetOItem(sendItem, localConfig.Globals.Type_Object);


            //    var selectedObjectRequest = new InterServiceMessage
            //    {
            //        ChannelId = Channels.ParameterList,
            //        SenderId = webSocketServiceAgent.EndpointId,
            //        OItems = new List<clsOntologyItem> { oItem }
            //    };

            //    webSocketServiceAgent.SendInterModMessage(selectedObjectRequest);

            //}
        }

        private void UnselectedObjectRows()
        {
            var sendItem = webSocketServiceAgent.Request["IdInstance"].ToString();
            if (string.IsNullOrEmpty(sendItem)) return;

            NodeIds_Selected.Remove(sendItem);
        
        }

        private void ToggleCheckObjectRow(bool isChecked)
        {
            if (listType == ListType.AttributeTypes)
            {
                var attributeType = oitemListViewModel1.ListAdapter.AttributeTypeViewItems.FirstOrDefault(attType => attType.IdItem == webSocketServiceAgent.Request["IdItem"].ToString());
                if (isChecked && !appliedItems.Any(appliedItem => appliedItem.ListItem == attributeType))
                {
                    appliedItems.Add(new AppliedItem
                    {
                        ListItem = attributeType,
                        ListType = listType
                    });
                }
                else if (!isChecked && appliedItems.Any(appliedItem => appliedItem.ListItem == attributeType))
                {
                    appliedItems.RemoveAll(appliedItem => appliedItem.ListItem == attributeType);
                }
                    
                

            }
            else if (listType == ListType.RelationTypes)
            {
                var relationtypeType = oitemListViewModel1.ListAdapter.RelationTypeViewItems.FirstOrDefault(relType => relType.IdItem == webSocketServiceAgent.Request["IdItem"].ToString());
                if (isChecked && !appliedItems.Any(appliedItem => appliedItem.ListItem == relationtypeType))
                {
                    appliedItems.Add(new AppliedItem
                    {
                        ListItem = relationtypeType,
                        ListType = listType
                    });
                }
                else if (!isChecked && appliedItems.Any(appliedItem => appliedItem.ListItem == relationtypeType))
                {
                    appliedItems.RemoveAll(appliedItem => appliedItem.ListItem == relationtypeType);
                }
                   
               

            }
            else if (listType == ListType.Instances)
            {
                var instanceItem = oitemListViewModel1.ListAdapter.InstanceViewItems.FirstOrDefault(instance => instance.IdItem == webSocketServiceAgent.Request["IdItem"].ToString());
                
                if (isChecked && !appliedItems.Any(appliedItem => appliedItem.ListItem == instanceItem))
                {
                    appliedItems.Add(new AppliedItem
                    {
                        ListItem = instanceItem,
                        ListType = listType
                    });
                }
                else if (!isChecked && appliedItems.Any(appliedItem => appliedItem.ListItem == instanceItem))
                {
                    appliedItems.RemoveAll(appliedItem => appliedItem.ListItem == instanceItem);
                }
                    
                

            }
            else if (listType == ListType.InstanceInstance_Conscious)
            {
                var leftRightItem = oitemListViewModel1.ListAdapter.ObjectObjectConsciousViewItems.FirstOrDefault(instance => instance.IdItem == webSocketServiceAgent.Request["IdItem"].ToString());
                
                
                if (isChecked && !appliedItems.Any(appliedItem => appliedItem.ListItem == leftRightItem))
                {
                    appliedItems.Add(new AppliedItem
                    {
                        ListItem = leftRightItem,
                        ListType = listType
                    });
                }
                else if (!isChecked && appliedItems.Any(appliedItem => appliedItem.ListItem == leftRightItem))
                {
                    appliedItems.RemoveAll(appliedItem => appliedItem.ListItem == leftRightItem);
                }
                    
                

            }
            else if (listType == ListType.InstanceInstance_Subconscious)
            {
                var rightLeftItem = oitemListViewModel1.ListAdapter.ObjectObjectSubconsciousViewItems.FirstOrDefault(instance => instance.IdItem == webSocketServiceAgent.Request["IdItem"].ToString());
                if (isChecked && !appliedItems.Any(appliedItem => appliedItem.ListItem == rightLeftItem))
                {
                    appliedItems.Add(new AppliedItem
                    {
                        ListItem = rightLeftItem,
                        ListType = listType
                    });
                }
                else if (!isChecked && appliedItems.Any(appliedItem => appliedItem.ListItem == rightLeftItem))
                {
                    appliedItems.RemoveAll(appliedItem => appliedItem.ListItem == rightLeftItem);
                }
                    
                

            }
            else if (listType == ListType.InstanceOther_Conscious)
            {
                var relItem = oitemListViewModel1.ListAdapter.ObjectOtherConsciousViewItems.FirstOrDefault(instance => instance.IdItem == webSocketServiceAgent.Request["IdItem"].ToString());
                if (isChecked && !appliedItems.Any(appliedItem => appliedItem.ListItem == relItem))
                {
                    appliedItems.Add(new AppliedItem
                    {
                        ListItem = relItem,
                        ListType = listType
                    });
                }
                else if (!isChecked && appliedItems.Any(appliedItem => appliedItem.ListItem == relItem))
                {
                    appliedItems.RemoveAll(appliedItem => appliedItem.ListItem == relItem);
                }
                
            }
            else if (listType == ListType.InstanceOther_Subconscious)
            {
                var relItem = oitemListViewModel1.ListAdapter.ObjectOtherSubconsciousViewItems.FirstOrDefault(instance => instance.IdItem == webSocketServiceAgent.Request["IdItem"].ToString());
                if (isChecked && !appliedItems.Any(appliedItem => appliedItem.ListItem == relItem))
                {
                    appliedItems.Add(new AppliedItem
                    {
                        ListItem = relItem,
                        ListType = listType
                    });
                }
                else if (!isChecked && appliedItems.Any(appliedItem => appliedItem.ListItem == relItem))
                {
                    appliedItems.RemoveAll(appliedItem => appliedItem.ListItem == relItem);
                }
                    
                

            }
            else if (listType == ListType.InstanceAttribute_Conscious)
            {
                var relItem = oitemListViewModel1.ListAdapter.ObjectAttributeViewItems.FirstOrDefault(instance => instance.IdItem == webSocketServiceAgent.Request["IdItem"].ToString());
                if (isChecked && !appliedItems.Any(appliedItem => appliedItem.ListItem == relItem))
                {
                    appliedItems.Add(new AppliedItem
                    {
                        ListItem = relItem,
                        ListType = listType
                    });
                }
                else if (!isChecked && appliedItems.Any(appliedItem => appliedItem.ListItem == relItem))
                {
                    appliedItems.RemoveAll(appliedItem => appliedItem.ListItem == relItem);
                }
                    
                
            }

            IsEnabled_Apply = appliedItems.Any();
        }

        private void AppliedObjectRows()
        {
            if (appliedItems.Any())
            {
                if (listType == ListType.AttributeTypes)
                {


                }
                else if (listType == ListType.RelationTypes)
                {

                }
                else if (listType == ListType.Instances)
                {
                    var oItems = appliedItems.Select(applItm => serviceAgent_Elastic.GetOItem(((InstanceViewItem)applItm.ListItem).IdInstance, localConfig.Globals.Type_Object)).ToList();
                    var interModMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.AppliedObjects,
                        OItems = oItems
                    };

                    webSocketServiceAgent.SendInterModMessage(interModMessage);
                }
                else if (listType == ListType.InstanceInstance_Conscious ||
                    listType == ListType.InstanceOther_Conscious)
                {
                    var oItems = appliedItems.Select(applItm => serviceAgent_Elastic.GetOItem(((ObjectObject_Conscious)applItm.ListItem).IdOther, localConfig.Globals.Type_Object)).ToList();
                    var interModMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.AppliedObjects,
                        OItems = oItems
                    };

                    webSocketServiceAgent.SendInterModMessage(interModMessage);
                }
                else if (listType == ListType.InstanceInstance_Subconscious ||
                    listType == ListType.InstanceOther_Subconscious)
                {
                    var oItems = appliedItems.Select(applItm => serviceAgent_Elastic.GetOItem(((ObjectObject_Subconscious)applItm.ListItem).IdObject, localConfig.Globals.Type_Object)).ToList();
                    var interModMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.AppliedObjects,
                        OItems = oItems
                    };

                    webSocketServiceAgent.SendInterModMessage(interModMessage);
                }
            }
        }

        private void NewItem()
        {
            if (listType == ListType.AttributeTypes)
            {


            }
            else if (listType == ListType.RelationTypes)
            {

            }
            else if (listType == ListType.Instances)
            {
                var view = ModuleDataExchanger.GetViewById("523c22bb4ea44b8da38259218e5c29fb");
                if (view == null) return;
                var url = "../" + view.NameCommandLineRun + "?Session=" + webSocketServiceAgent.DataText_SessionId;
                url += "&ItemType=" + localConfig.Globals.Type_Object;
                url += "&ParentId=" + oItem.GUID;
                Url_NewItem = url;
                Url_NewItem = "";
            }
            else if (listType == ListType.InstanceInstance_Conscious)
            {

            }
            else if (listType == ListType.InstanceInstance_Subconscious)
            {

            }
            else if (listType == ListType.InstanceOther_Conscious)
            {

            }
            else if (listType == ListType.InstanceOther_Subconscious)
            {

            }
            else if (listType == ListType.InstanceAttribute_Conscious)
            {

            }
        }

        private void SaveChangedName()
        {
            if (listType == ListType.AttributeTypes)
            {
                var isExisting = serviceAgent_Elastic.IsExisting(DataText_NameValue, true);

                var save = false;
                if (isExisting.GUID == localConfig.Globals.LState_Exists.GUID)
                {
                    if (isExisting.OList_Rel.First().GUID == attributeTypeChangeItem.IdAttributeType)
                    {
                        save = true;
                    }
                    else
                    {
                        save = false;
                    }

                }
                else if (isExisting.GUID == localConfig.Globals.LState_Nothing.GUID)
                {
                    save = true;
                }

                if (save)
                {
                    var itemToSave = new clsOntologyItem
                    {
                        GUID = attributeTypeChangeItem.IdAttributeType,
                        Name = DataText_NameValue,
                        GUID_Parent = attributeTypeChangeItem.IdDataType,
                        Type = localConfig.Globals.Type_AttributeType
                    };
                    var result = serviceAgent_Elastic.SaveItem(itemToSave);
                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        CellItem_ChangedItem = new JqxCellItem
                        {
                            Id = CellItem_CellDoubleClicked.Id,
                            RowId = CellItem_CellDoubleClicked.RowId,
                            ColumnName = CellItem_CellDoubleClicked.ColumnName,
                            Value = DataText_NameValue
                        };

                        webSocketServiceAgent.SendCommand("CloseEditName");
                    }
                    else
                    {
                        Text_EditMessageName = "Fehler!!!";
                    }
                }
            }
            else if (listType == ListType.RelationTypes)
            {
                var isExisting = serviceAgent_Elastic.IsExisting(DataText_NameValue, false);
                var save = false;
                if (isExisting.GUID == localConfig.Globals.LState_Exists.GUID)
                {
                    if (isExisting.OList_Rel.First().GUID == relationTypeChangeItem.IdRelationType)
                    {
                        save = true;
                    }
                    else
                    {
                        save = false;
                    }

                }
                else if (isExisting.GUID == localConfig.Globals.LState_Nothing.GUID)
                {
                    save = true;
                }

                if (save)
                {
                    var itemToSave = new clsOntologyItem
                    {
                        GUID = relationTypeChangeItem.IdRelationType,
                        Name = DataText_NameValue,
                        Type = localConfig.Globals.Type_RelationType
                    };
                    var result = serviceAgent_Elastic.SaveItem(itemToSave);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        CellItem_ChangedItem = new JqxCellItem
                        {
                            Id = CellItem_CellDoubleClicked.Id,
                            RowId = CellItem_CellDoubleClicked.RowId,
                            ColumnName = CellItem_CellDoubleClicked.ColumnName,
                            Value = DataText_NameValue
                        };

                        webSocketServiceAgent.SendCommand("CloseEditName");
                    }
                    else
                    {
                        Text_EditMessageName = "Fehler!!!";
                    }
                }
            }
            else if (listType == ListType.Instances)
            {
                var isExisting = serviceAgent_Elastic.IsExisting(DataText_NameValue, instanceChangeItem.IdClass);
                var save = false;
                if (isExisting.GUID == localConfig.Globals.LState_Exists.GUID)
                {
                    if (isExisting.OList_Rel.First().GUID == instanceChangeItem.IdInstance)
                    {
                        save = true;
                    }
                    else
                    {
                        save = false;
                    }

                }
                else if (isExisting.GUID == localConfig.Globals.LState_Nothing.GUID)
                {
                    save = true;
                }

                if (save)
                {
                    var itemToSave = new clsOntologyItem
                    {
                        GUID = instanceChangeItem.IdInstance,
                        Name = DataText_NameValue,
                        GUID_Parent = instanceChangeItem.IdClass,
                        Type = localConfig.Globals.Type_Object
                    };
                    var result = serviceAgent_Elastic.SaveItem(itemToSave);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        CellItem_ChangedItem = new JqxCellItem
                        {
                            Id = CellItem_CellDoubleClicked.Id,
                            RowId = CellItem_CellDoubleClicked.RowId,
                            ColumnName = CellItem_CellDoubleClicked.ColumnName,
                            Value = DataText_NameValue
                        };

                        webSocketServiceAgent.SendCommand("CloseEditName");
                    }
                    else
                    {
                        Text_EditMessageName = "Fehler!!!";
                    }
                }
            }
        }

        private void SaveOrderId()
        {
            if (CellItem_CellDoubleClicked == null) return;

            if (listType == ListType.InstanceInstance_Conscious)
            {
                var leftRightItem = oitemListViewModel1.ListAdapter.ObjectObjectConsciousViewItems.FirstOrDefault(instance => instance.IdItem == CellItem_CellDoubleClicked.Id);

                if ((int)leftRightItem.OrderId != Int_OrderIdValue)
                {
                    var saveItem = leftRightItem.proxyItem.Clone();
                    saveItem.OrderID = Int_OrderIdValue;
                    var result = serviceAgent_Elastic.SaveRelationItem(saveItem);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        leftRightItem.OrderId = Int_OrderIdValue;
                        CellItem_ChangedItem = new JqxCellItem
                        {
                            Id = leftRightItem.IdItem,
                            RowId = CellItem_CellDoubleClicked.RowId,
                            ColumnName = CellItem_CellDoubleClicked.ColumnName,
                            Value = Int_OrderIdValue.ToString()
                        };
                    }
                    webSocketServiceAgent.SendCommand("CloseEditOrderId");
                }

            }
            else if (listType == ListType.InstanceInstance_Subconscious)
            {
                var rightLeftItem = oitemListViewModel1.ListAdapter.ObjectObjectSubconsciousViewItems.FirstOrDefault(instance => instance.IdItem == CellItem_CellDoubleClicked.Id);

                if ((int)rightLeftItem.OrderId != Int_OrderIdValue)
                {
                    var saveItem = rightLeftItem.proxyItem.Clone();
                    saveItem.OrderID = Int_OrderIdValue;
                    var result = serviceAgent_Elastic.SaveRelationItem(saveItem);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        rightLeftItem.OrderId = Int_OrderIdValue;
                        CellItem_ChangedItem = new JqxCellItem
                        {
                            Id = rightLeftItem.IdItem,
                            RowId = CellItem_CellDoubleClicked.RowId,
                            ColumnName = CellItem_CellDoubleClicked.ColumnName,
                            Value = Int_OrderIdValue.ToString()
                        };
                    }
                    webSocketServiceAgent.SendCommand("CloseEditOrderId");
                }
            }
            else if (listType == ListType.InstanceOther_Conscious)
            {
                var relItem = oitemListViewModel1.ListAdapter.ObjectOtherConsciousViewItems.FirstOrDefault(instance => instance.IdItem == CellItem_CellDoubleClicked.Id);

                if ((int)relItem.OrderId != Int_OrderIdValue)
                {
                    var saveItem = relItem.proxyItem.Clone();
                    saveItem.OrderID = Int_OrderIdValue;
                    var result = serviceAgent_Elastic.SaveRelationItem(saveItem);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        relItem.OrderId = Int_OrderIdValue;
                        CellItem_ChangedItem = new JqxCellItem
                        {
                            Id = relItem.IdItem,
                            RowId = CellItem_CellDoubleClicked.RowId,
                            ColumnName = CellItem_CellDoubleClicked.ColumnName,
                            Value = Int_OrderIdValue.ToString()
                        };
                    }
                    webSocketServiceAgent.SendCommand("CloseEditOrderId");
                }
            }
            else if (listType == ListType.InstanceOther_Subconscious)
            {
                var relItem = oitemListViewModel1.ListAdapter.ObjectOtherSubconsciousViewItems.FirstOrDefault(instance => instance.IdItem == CellItem_CellDoubleClicked.Id);

                if ((int)relItem.OrderId != Int_OrderIdValue)
                {
                    var saveItem = relItem.proxyItem.Clone();
                    saveItem.OrderID = Int_OrderIdValue;
                    var result = serviceAgent_Elastic.SaveRelationItem(saveItem);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        relItem.OrderId = Int_OrderIdValue;
                        CellItem_ChangedItem = new JqxCellItem
                        {
                            Id = relItem.IdItem,
                            RowId = CellItem_CellDoubleClicked.RowId,
                            ColumnName = CellItem_CellDoubleClicked.ColumnName,
                            Value = Int_OrderIdValue.ToString()
                        };
                    }
                    webSocketServiceAgent.SendCommand("CloseEditOrderId");
                }
            }
            else if (listType == ListType.InstanceAttribute_Conscious)
            {

            }
        }

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {
                

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == "SelectedObjectRows")
                {
                    SelectedObjectRows();


                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "UnselectedObjectRow")
                {
                    UnselectedObjectRows();
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "AppliedObjectRow")
                {
                    ToggleCheckObjectRow(true);
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "UnAppliedObjectRow")
                {
                    ToggleCheckObjectRow(false);
                    
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "AppliedObjectRows")
                {


                    AppliedObjectRows();
                    Text_HideWaitViewItemId = "applyItem";

                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "NewItem")
                {
                    NewItem();
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "SaveName")
                {
                    SaveChangedName();   
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "SaveOrderId")
                {
                    SaveOrderId();   
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "OpenedEditNameItemWindow" ||
                    webSocketServiceAgent.Command_RequestedCommand == "OpenedEditOrderIdItemWindow")
                {
                    editInProgress = true;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "ClosedEditNameItemWindow" ||
                    webSocketServiceAgent.Command_RequestedCommand == "ClosedEditOrderIdItemWindow")
                {
                    editInProgress = false;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "DeleteItem")
                {
                    DeleteItems();
                }
                

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {
                    if (changedItem.ViewItemId == "grid" && changedItem.ViewItemType == "SelectedIndex")
                    {
                        CellItem_CellSelected = Newtonsoft.Json.JsonConvert.DeserializeObject<JqxCellItem>(changedItem.ViewItemValue.ToString());
                    }
                    else if (changedItem.ViewItemId == "grid" && changedItem.ViewItemType == "DoubleClick")
                    {
                        CellItem_CellDoubleClicked = Newtonsoft.Json.JsonConvert.DeserializeObject<JqxCellItem>(changedItem.ViewItemValue.ToString());
                        
                    }
                    else if (changedItem.ViewItemId == "inpValueEditName" && changedItem.ViewItemType == "Content")
                    {
                        timerNameChange.Stop();
                        Text_EditMessageName = translationController.Text_Check;
                        IsEnabled_SaveName = false;
                        timerNameChange.Start();
                    }
                });
            }
            else if (e.PropertyName == NotifyChanges.Websocket_ViewArguments)
            {
                if (!webSocketServiceAgent.ViewArguments.Any()) return;

                ParamItemCheck();
            }
            else if (e.PropertyName == NotifyChanges.Websocket_ClassArgument)
            {
                if (listType != ListType.Instances) return;
                
                DataText_ClassId = webSocketServiceAgent.ClassArgument.Value;
                if (oItem == null)
                {
                    oItem = serviceAgent_Elastic.GetOItem(DataText_ClassId, localConfig.Globals.Type_Class);
                    if (oItem != null)
                    {
                        IsVisible_Apply = true;
                        LocalStateMachine.ItemListLoadRequest();
                        oitemListViewModel1.Initialize_ItemList(ListType.Instances, new clsOntologyItem { GUID_Parent = oItem.GUID }, true, DestinationType.None);
                    }
                }

            }
            else if (e.PropertyName == NotifyChanges.Websocket_DedicatedSenderArgument)
            {
                if (webSocketServiceAgent.DedicatedSenderArgument != null)
                {
                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.ReceiverOfDedicatedSender,
                        ReceiverId = webSocketServiceAgent.DedicatedSenderArgument.Value
                    };

                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }
            }


        }

        private void DeleteItems()
        {
            if (appliedItems.Any())
            {
                if (listType == ListType.AttributeTypes)
                {


                }
                else if (listType == ListType.RelationTypes)
                {

                }
                else if (listType == ListType.Instances)
                {
                   
                }
                else if (listType == ListType.InstanceInstance_Conscious)
                {
                    var oItems = appliedItems.Select(appliedItem => new clsObjectRel { ID_Object = ((ObjectObject_Conscious)appliedItem.ListItem).IdObject,
                        ID_Other = ((ObjectObject_Conscious)appliedItem.ListItem).IdOther,
                        ID_RelationType = ((ObjectObject_Conscious)appliedItem.ListItem).IdRelationType
                    }).ToList();

                    var resultTask = serviceAgent_Elastic.DeleteObjectRels(oItems);


                }
                else if (listType == ListType.InstanceInstance_Omni)
                {
                    var oItems = appliedItems.Select(appliedItem => new clsObjectRel
                    {
                        ID_Object = ((ObjectObject_Omni)appliedItem.ListItem).IdObject,
                        ID_Other = ((ObjectObject_Omni)appliedItem.ListItem).IdOther,
                        ID_RelationType = ((ObjectObject_Omni)appliedItem.ListItem).IdRelationType
                    }).ToList();

                    var resultTask = serviceAgent_Elastic.DeleteObjectRels(oItems);
                }
                else if (listType == ListType.InstanceInstance_Subconscious)
                {
                    var oItems = appliedItems.Select(appliedItem => new clsObjectRel
                    {
                        ID_Object = ((ObjectObject_Subconscious)appliedItem.ListItem).IdObject,
                        ID_Other = ((ObjectObject_Subconscious)appliedItem.ListItem).IdOther,
                        ID_RelationType = ((ObjectObject_Subconscious)appliedItem.ListItem).IdRelationType
                    }).ToList();

                    var resultTask = serviceAgent_Elastic.DeleteObjectRels(oItems);
                }
                else if (listType == ListType.InstanceOther_Conscious)
                {
                    var oItems = appliedItems.Select(appliedItem => new clsObjectRel
                    {
                        ID_Object = ((ObjectObject_Conscious)appliedItem.ListItem).IdObject,
                        ID_Other = ((ObjectObject_Conscious)appliedItem.ListItem).IdOther,
                        ID_RelationType = ((ObjectObject_Conscious)appliedItem.ListItem).IdRelationType
                    }).ToList();

                    var resultTask = serviceAgent_Elastic.DeleteObjectRels(oItems);
                }
                else if (listType == ListType.InstanceOther_Subconscious)
                {
                    var oItems = appliedItems.Select(appliedItem => new clsObjectRel
                    {
                        ID_Object = ((ObjectObject_Subconscious)appliedItem.ListItem).IdObject,
                        ID_Other = ((ObjectObject_Subconscious)appliedItem.ListItem).IdOther,
                        ID_RelationType = ((ObjectObject_Subconscious)appliedItem.ListItem).IdRelationType
                    }).ToList();

                    var resultTask = serviceAgent_Elastic.DeleteObjectRels(oItems);
                }
            }
        }

        private void TimerSelection_Elapsed(object sender, ElapsedEventArgs e)
        {
            timerSelection.Stop();

            NodeIds_Selected = selectedNodeIds;
            if (NodeIds_Selected.Any())
            {
                var nodeIdLast = NodeIds_Selected.LastOrDefault();
                if (nodeIdLast == null) return;

                if (listType == ListType.AttributeTypes)
                {
                    var attributeType = oitemListViewModel1.ListAdapter.AttributeTypeViewItems.FirstOrDefault(attType => attType.IdItem == nodeIdLast);
                    

                }
                else if (listType == ListType.RelationTypes)
                {
                    var relationtypeType = oitemListViewModel1.ListAdapter.RelationTypeViewItems.FirstOrDefault(relType => relType.IdItem == nodeIdLast);
                    
                }
                else if (listType == ListType.Instances)
                {
                    var instanceItem = oitemListViewModel1.ListAdapter.InstanceViewItems.FirstOrDefault(instance => instance.IdItem == nodeIdLast);
                    var oItem = new clsOntologyItem
                    {
                        GUID = instanceItem.IdInstance,
                        Name = instanceItem.NameInstance,
                        GUID_Parent = instanceItem.IdClass,
                        Type = localConfig.Globals.Type_Object
                    };
                    var selectedObjectRequest = new InterServiceMessage
                    {
                        ChannelId = Channels.ParameterList,
                        OItems = new List<clsOntologyItem>
                        {
                            oItem
                        }
                    };
                    webSocketServiceAgent.SendInterModMessage(selectedObjectRequest);    
                }
                else if (listType == ListType.InstanceInstance_Conscious)
                {
                    var leftRightItem = oitemListViewModel1.ListAdapter.ObjectObjectConsciousViewItems.FirstOrDefault(instance => instance.IdItem == nodeIdLast);
                    
                }
                else if (listType == ListType.InstanceInstance_Subconscious)
                {
                    var rightLeftItem = oitemListViewModel1.ListAdapter.ObjectObjectSubconsciousViewItems.FirstOrDefault(instance => instance.IdItem == nodeIdLast);
                    
                }
                else if (listType == ListType.InstanceOther_Conscious)
                {
                    var relItem = oitemListViewModel1.ListAdapter.ObjectOtherConsciousViewItems.FirstOrDefault(instance => instance.IdItem == nodeIdLast);
                    
                }
                else if (listType == ListType.InstanceOther_Subconscious)
                {
                    var relItem = oitemListViewModel1.ListAdapter.ObjectOtherSubconsciousViewItems.FirstOrDefault(instance => instance.IdItem == nodeIdLast);
                    
                }
                else if (listType == ListType.InstanceAttribute_Conscious)
                {
                    var relItem = oitemListViewModel1.ListAdapter.ObjectAttributeViewItems.FirstOrDefault(instance => instance.IdItem == nodeIdLast);
                    
                }

                
                
            }

        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void ReceivedObject()
        {
            if (listType == ListType.InstanceInstance_Omni)
            {
                oItemClass = serviceAgent_Elastic.GetOItem(oItem.GUID_Parent, localConfig.Globals.Type_Class);
                Text_View = oItem.Name + " (" + oItemClass.Name + ")";
                LocalStateMachine.ItemListLoadRequest();
                oitemListViewModel1.Initialize_ItemList(ListType.InstanceInstance_Omni, new clsObjectRel { ID_Object = oItem.GUID, ID_RelationType = oItem.GUID_Relation, ID_Parent_Other = oItem.GUID_Related });
            }
            else if (listType == ListType.InstanceAttribute_Conscious)
            {
                oItemClass = serviceAgent_Elastic.GetOItem(oItem.GUID_Parent, localConfig.Globals.Type_Class);
                Text_View = oItem.Name + " (" + oItemClass.Name + ")";
                LocalStateMachine.ItemListLoadRequest();
                oitemListViewModel1.Initialize_ItemList(new clsObjectAtt { ID_Object = oItem.GUID });
            }
        }

        private void ApplyFilter(OFilterItem filterItem)
        {
            var classItem = serviceAgent_Elastic.GetOItem(filterItem.IdClassItem, localConfig.Globals.Type_Class);
            var relationTypeItem = serviceAgent_Elastic.GetOItem(filterItem.IdRelationTypeItem, localConfig.Globals.Type_RelationType);
            clsOntologyItem directionItem;

            if (filterItem.IdDirectionItem == localConfig.Globals.Direction_LeftRight.GUID)
            {
                directionItem = localConfig.Globals.Direction_LeftRight;
            }
            else
            {
                directionItem = localConfig.Globals.Direction_RightLeft;

            }

            clsOntologyItem objectItem = null;

            if (!string.IsNullOrEmpty(filterItem.IdObjectItem))
            {
                objectItem = serviceAgent_Elastic.GetOItem(filterItem.IdObjectItem, localConfig.Globals.Type_Object);
            }

            oitemListViewModel1.Initialize_AdvancedFilter(ListType.Instances, oItemClass, classItem, objectItem, relationTypeItem, directionItem);
        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (editInProgress) return;
            if (!IsToggled_Listen) return;
            omniRelationItems.Clear();
            appliedItems.Clear();
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.ParameterList && message.GenericParameterItems != null)
            {
               
                
            }
            else if (message.ChannelId == Channels.SelectedClassNode)
            {
                
                var oItemMessage = message.OItems.LastOrDefault();
                if (oItemMessage == null) return;
                if (oItem != null && oItemMessage.GUID == oItem.GUID) return;

                oItem = oItemMessage;

                if (oItem == null) return;
                if (listType != ListType.Instances) return;

                IsVisible_Apply = true;
                LocalStateMachine.ItemListLoadRequest();
                oitemListViewModel1.Initialize_ItemList(ListType.Instances, new clsOntologyItem { GUID_Parent = oItem.GUID }, true, DestinationType.None);
                oItemClass = serviceAgent_Elastic.GetOItem(oItem.GUID, localConfig.Globals.Type_Class);
                Text_View = "(" + oItemClass.Name + ")";
                
            }
            else if (message.ChannelId == Channels.ParameterList)
            {
                var oItemMessage = message.OItems.LastOrDefault();
                if (oItemMessage == null) return;
                if (oItem != null && oItemMessage.GUID == oItem.GUID) return;

                oItem = message.OItems.LastOrDefault();

                if (oItem == null) return;

                ReceivedObject();
            }
            else if (message.ChannelId == Channels.SelectedRelationNode && 
                listType != ListType.Instances &&
                listType != ListType.AttributeTypes &&
                listType != ListType.RelationTypes &&
                listType != ListType.Classes)
            {
                var oItemMessage = message.OItems.LastOrDefault();
                if (oItemMessage == null) return;

                oItem = message.OItems.LastOrDefault();

                if (message.ParameterIdentification == typeof(ClassAttributeNode).ToString())
                {
                
                    var clsAttNode = Newtonsoft.Json.JsonConvert.DeserializeObject<ClassAttributeNode>(message.GenericParameterItems.First().ToString());

                    if (classAttributeNode != null && clsAttNode.IdClass == classAttributeNode.IdClass && clsAttNode.IdAttributeType == classAttributeNode.IdAttributeType) return;

                    classAttributeNode = clsAttNode;
                    
                    if (listType == ListType.InstanceAttribute_Conscious)
                    {
                        Text_View = classAttributeNode.NameAttributeType + " (" + classAttributeNode.NameClass + ")";
                        IsVisible_Apply = true;
                        LocalStateMachine.ItemListLoadRequest();
                        oitemListViewModel1.Initialize_ItemList(new clsObjectAtt { ID_Object = oItem.GUID, ID_AttributeType = classAttributeNode.IdAttributeType });
                    }
                }
                else if (message.ParameterIdentification == typeof(ObjectRelNode).ToString())
                {
                
                    var objRelNode = Newtonsoft.Json.JsonConvert.DeserializeObject<ObjectRelNode>(message.GenericParameterItems.First().ToString());
                    if (objectRelNode != null && objRelNode.IdLeft == objectRelNode.IdLeft && objRelNode.IdRelationType == objectRelNode.IdRelationType && objRelNode.IdRight == objectRelNode.IdRight) return;
                    objectRelNode = objRelNode;

                    BeginLoadItems();
                }
                
            }
            else if (message.ChannelId == Channels.AddedAttributes && listType == ListType.AttributeTypes)
            {

                var datatypeItems = message.OItems.Select(attType =>
                {
                    if (attType.GUID_Parent == localConfig.Globals.DType_Bool.GUID)
                    {
                        return localConfig.Globals.DType_Bool.Clone();
                    }
                    else if (attType.GUID_Parent == localConfig.Globals.DType_DateTime.GUID)
                    {
                        return localConfig.Globals.DType_DateTime.Clone();
                    }
                    else if (attType.GUID_Parent == localConfig.Globals.DType_Int.GUID)
                    {
                        return localConfig.Globals.DType_Int.Clone();
                    }
                    else if (attType.GUID_Parent == localConfig.Globals.DType_Real.GUID)
                    {
                        return localConfig.Globals.DType_Real.Clone();
                    }
                    else if (attType.GUID_Parent == localConfig.Globals.DType_String.GUID)
                    {
                        return localConfig.Globals.DType_String.Clone();
                    }

                    return null;
                }).ToList();

                var viewItems = (from attType in message.OItems
                                 join dataType in datatypeItems on attType.GUID_Parent equals dataType.GUID
                                 select new AttributeTypeViewItem(attType, dataType)).ToList();

                var viewItem = GridFactory.CreateViewItem(viewItems.ToList<object>(), typeof(AttributeTypeViewItem), "grid", ViewItemClass.Grid.ToString(), ViewItemType.AddRow.ToString());

                webSocketServiceAgent.SendViewItems(new List<ViewItem> { viewItem });

                oitemListViewModel1.ListAdapter.AttributeTypeViewItems.AddRange(viewItems);
                
            }
            else if (message.ChannelId == Channels.AddedRelationTypes && listType == ListType.RelationTypes)
            {

                var viewItems = message.OItems.Select(oItem => new RelationTypeViewItem(oItem)).ToList();

                var viewItem = GridFactory.CreateViewItem(viewItems.ToList<object>(), typeof(RelationTypeViewItem), "grid", ViewItemClass.Grid.ToString(), ViewItemType.AddRow.ToString());

                webSocketServiceAgent.SendViewItems(new List<ViewItem> { viewItem });

                oitemListViewModel1.ListAdapter.RelationTypeViewItems.AddRange(viewItems);

            }
            else if (message.ChannelId == Channels.AddedClasses && listType == ListType.Classes)
            {
                var classViewItems = message.OItems.Select(oItem =>
                {
                    return new ClassViewItem(oItem, serviceAgent_Elastic.GetOItem(oItem.GUID_Parent, localConfig.Globals.Type_Class));
                }).ToList();

                var viewItem = GridFactory.CreateViewItem(classViewItems.ToList<object>(), typeof(ClassViewItem), "grid", ViewItemClass.Grid.ToString(), ViewItemType.AddRow.ToString());

                webSocketServiceAgent.SendViewItems(new List<ViewItem> { viewItem });

                oitemListViewModel1.ListAdapter.ClassViewItems.AddRange(classViewItems);

            }
            else if (message.ChannelId == Channels.AddedObjects && listType == ListType.Instances)
            {
                var objectViewItems = message.OItems.Select(oItem =>
                {
                    return new InstanceViewItem(oItem, serviceAgent_Elastic.GetOItem(oItem.GUID_Parent, localConfig.Globals.Type_Class));
                }).ToList();

                var viewItem = GridFactory.CreateViewItem(objectViewItems.ToList<object>(), typeof(InstanceViewItem), "grid", ViewItemClass.Grid.ToString(), ViewItemType.AddRow.ToString());

                webSocketServiceAgent.SendViewItems(new List<ViewItem> { viewItem });
                oitemListViewModel1.ListAdapter.InstanceViewItems.AddRange(objectViewItems);
            }
            else if (message.ChannelId == Channels.ApplyFilter && listType == ListType.Instances )
            {
                var genericParameter = message.GenericParameterItems.FirstOrDefault();
                if (genericParameter == null) return;

                var filterToApply = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(genericParameter.ToString());

                if (filterToApply == null) return;
                OFilterItem filterItem = new OFilterItem
                {
                    IdClassItem = filterToApply.IdClassItem,
                    IdRelationTypeItem = filterToApply.IdRelationTypeItem,
                    IdDirectionItem = filterToApply.IdDirectionItem,
                    IdObjectItem = filterToApply.IdObject
                };
                ApplyFilter(filterItem);
            }

        }


        private void BeginLoadItems()
        {
            

            if (objectRelNode.IdLeft == oItem.GUID_Parent && (objectRelNode.NodeType == NodeType.NodeForwardFormal || objectRelNode.NodeType == NodeType.NodeForwardInformal))
            {
                if (listType == ListType.InstanceInstance_Conscious && objectRelNode.NodeType == NodeType.NodeForwardFormal)
                {
                    Text_View = oItem.Name + " (" + objectRelNode.NodeName + ")";
                    IsVisible_Apply = true;
                    LocalStateMachine.ItemListLoadRequest();
                    oitemListViewModel1.Initialize_ItemList(ListType.InstanceInstance_Conscious, new clsObjectRel { ID_Object = oItem.GUID, ID_RelationType = objectRelNode.IdRelationType, ID_Parent_Other = objectRelNode.IdRight });
                    
                }
                else if (listType == ListType.InstanceInstance_Omni)
                {
                    Text_View = oItem.Name + " (" + objectRelNode.NodeName + ")";
                    LocalStateMachine.ItemListLoadRequest();
                    oitemListViewModel1.Initialize_ItemList(ListType.InstanceInstance_Omni, new clsObjectRel { ID_Object = oItem.GUID, ID_RelationType = objectRelNode.IdRelationType, ID_Parent_Other = objectRelNode.IdRight });
                }
                else if (listType == ListType.InstanceOther_Conscious && objectRelNode.NodeType == NodeType.NodeForwardInformal)
                {
                    Text_View = oItem.Name + " (" + objectRelNode.NodeName + ")";
                    IsVisible_Apply = true;
                    LocalStateMachine.ItemListLoadRequest();
                    oitemListViewModel1.Initialize_ItemList(ListType.InstanceOther_Conscious, new clsObjectRel { ID_Object = oItem.GUID, ID_RelationType = objectRelNode.IdRelationType });
                }

                
            }
            else if (objectRelNode.IdRight == oItem.GUID_Parent && (objectRelNode.NodeType == NodeType.NodeBackwardFormal || objectRelNode.NodeType == NodeType.NodeBackwardInformal))
            {
                if (listType == ListType.InstanceInstance_Subconscious && objectRelNode.NodeType == NodeType.NodeBackwardFormal)
                {
                    Text_View = oItem.Name + " (" + objectRelNode.NodeName + ")";
                    IsVisible_Apply = true;
                    LocalStateMachine.ItemListLoadRequest();
                    oitemListViewModel1.Initialize_ItemList(ListType.InstanceInstance_Subconscious, new clsObjectRel { ID_Other = oItem.GUID, ID_RelationType = objectRelNode.IdRelationType, ID_Parent_Object = objectRelNode.IdLeft });
                }
                else if (listType == ListType.InstanceInstance_Omni)
                {
                    Text_View = oItem.Name + " (" + objectRelNode.NodeName + ")";
                    LocalStateMachine.ItemListLoadRequest();
                    oitemListViewModel1.Initialize_ItemList(ListType.InstanceInstance_Omni, new clsObjectRel { ID_Object = oItem.GUID, ID_RelationType = objectRelNode.IdRelationType, ID_Parent_Other = objectRelNode.IdLeft });
                }
                else if (listType == ListType.InstanceOther_Conscious && objectRelNode.NodeType == NodeType.NodeBackwardInformal)
                {
                    Text_View = oItem.Name + " (" + objectRelNode.NodeName + ")";
                    IsVisible_Apply = true;
                    LocalStateMachine.ItemListLoadRequest();
                    oitemListViewModel1.Initialize_ItemList(ListType.InstanceOther_Subconscious, new clsObjectRel { ID_Object = oItem.GUID, ID_RelationType = objectRelNode.IdRelationType, ID_Parent_Other = objectRelNode.IdRight });
                }

                
            }
            else if (objectRelNode.IdRight == oItem.GUID)
            {
                if (listType == ListType.InstanceOther_Subconscious && objectRelNode.NodeType == NodeType.NodeBackwardInformal)
                {
                    Text_View = oItem.Name + " (" + objectRelNode.NodeName + ")";
                    LocalStateMachine.ItemListLoadRequest();
                    oitemListViewModel1.Initialize_ItemList(ListType.InstanceOther_Subconscious, new clsObjectRel { ID_Other = oItem.GUID, ID_RelationType = objectRelNode.IdRelationType });
                }
                


            }
            else if (objectRelNode.IdLeft == oItem.GUID && (objectRelNode.NodeType == NodeType.NodeForwardFormal || objectRelNode.NodeType == NodeType.NodeForwardInformal))
            {
                if (listType == ListType.InstanceInstance_Conscious && objectRelNode.NodeType == NodeType.NodeForwardFormal)
                {
                    Text_View = oItem.Name + " (" + objectRelNode.NodeName + ")";
                    IsVisible_Apply = true;
                    LocalStateMachine.ItemListLoadRequest();
                    oitemListViewModel1.Initialize_ItemList(ListType.InstanceInstance_Conscious, new clsObjectRel { ID_Object = oItem.GUID, ID_RelationType = objectRelNode.IdRelationType, ID_Parent_Other = objectRelNode.IdRight });

                }
                else if (listType == ListType.InstanceInstance_Omni)
                {
                    Text_View = oItem.Name + " (" + objectRelNode.NodeName + ")";
                    LocalStateMachine.ItemListLoadRequest();
                    oitemListViewModel1.Initialize_ItemList(ListType.InstanceInstance_Omni, new clsObjectRel { ID_Object = oItem.GUID, ID_RelationType = objectRelNode.IdRelationType, ID_Parent_Other = objectRelNode.IdRight });
                }
                else if (listType == ListType.InstanceOther_Conscious && objectRelNode.NodeType == NodeType.NodeForwardInformal)
                {
                    Text_View = oItem.Name + " (" + objectRelNode.NodeName + ")";
                    IsVisible_Apply = true;
                    LocalStateMachine.ItemListLoadRequest();
                    oitemListViewModel1.Initialize_ItemList(ListType.InstanceOther_Conscious, new clsObjectRel { ID_Object = oItem.GUID, ID_RelationType = objectRelNode.IdRelationType, ID_Parent_Other = objectRelNode.IdRight });
                }


            }
            else if (objectRelNode.IdRight == oItem.GUID && (objectRelNode.NodeType == NodeType.NodeBackwardFormal || objectRelNode.NodeType == NodeType.NodeBackwardInformal))
            {
                if (listType == ListType.InstanceInstance_Subconscious && objectRelNode.NodeType == NodeType.NodeBackwardFormal)
                {
                    Text_View = oItem.Name + " (" + objectRelNode.NodeName + ")";
                    IsVisible_Apply = true;
                    LocalStateMachine.ItemListLoadRequest();
                    oitemListViewModel1.Initialize_ItemList(ListType.InstanceInstance_Subconscious, new clsObjectRel { ID_Other = oItem.GUID, ID_RelationType = objectRelNode.IdRelationType, ID_Parent_Object = objectRelNode.IdLeft });
                }
                else if (listType == ListType.InstanceInstance_Omni)
                {
                    Text_View = oItem.Name + " (" + objectRelNode.NodeName + ")";
                    LocalStateMachine.ItemListLoadRequest();
                    oitemListViewModel1.Initialize_ItemList(ListType.InstanceInstance_Omni, new clsObjectRel { ID_Object = oItem.GUID, ID_RelationType = objectRelNode.IdRelationType, ID_Parent_Other = objectRelNode.IdLeft });
                }
                else if (listType == ListType.InstanceOther_Conscious && objectRelNode.NodeType == NodeType.NodeBackwardInformal)
                {
                    Text_View = oItem.Name + " (" + objectRelNode.NodeName + ")";
                    IsVisible_Apply = true;
                    LocalStateMachine.ItemListLoadRequest();
                    oitemListViewModel1.Initialize_ItemList(ListType.InstanceOther_Subconscious, new clsObjectRel { ID_Object = oItem.GUID, ID_RelationType = objectRelNode.IdRelationType, ID_Parent_Other = objectRelNode.IdRight });
                }


            }
        }

        private void OitemListViewModel2_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            IsVisible_Apply = false;
            IsEnabled_Apply = false;

            if (e.PropertyName == "Result_ListLoader" && listType == ListType.InstanceInstance_Omni)
            {

                if (oitemListViewModel2.ListAdapter.Result_ListLoader.GUID == localConfig.Globals.LState_Success.GUID)
                {

                    dataSessionFile = webSocketServiceAgent.RequestWriteStream(fileNameData);
                    if (dataSessionFile == null) return;
                    if (dataSessionFile.FileUri == null)
                    {
                        return;
                    }

                    if (dataSessionFile.StreamWriter == null)
                    {

                        return;
                    }

                    var result = localConfig.Globals.LState_Success.Clone();

                    using (dataSessionFile.StreamWriter)
                    {

                        omniRelationItems.AddRange(oitemListViewModel2.ListAdapter.ObjectObjectOmniViewItems);
                        result = objectRelOmniFactory.CreateGridDataJsonOfModel(omniRelationItems, dataSessionFile.StreamWriter, IsVisible_Apply) ? localConfig.Globals.LState_Success.Clone() : localConfig.Globals.LState_Error.Clone();
                       


                    }
                    if (result.GUID == localConfig.Globals.LState_Error.GUID)
                    {

                        return;
                    }

                    webSocketServiceAgent.SendCommand("refreshGrid");

                    IsVisible_Apply = true;

                }

            }
        }

        private void ViewModelWinFormsOItemList_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            IsVisible_Apply = false;
            IsEnabled_Apply = false;

            if (!IsSuccessful_Login) return;
            if (e.PropertyName == "Result_ListLoader" && listType != ListType.InstanceInstance_Omni)
            {
                try
                {
                    if (oitemListViewModel1.ListAdapter.Result_ListLoader.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        if (string.IsNullOrEmpty(fileNameData))
                        {
                            fileNameData = Guid.NewGuid().ToString() + ".json";
                        }

                        dataSessionFile = webSocketServiceAgent.RequestWriteStream(fileNameData);

                        if (dataSessionFile == null || dataSessionFile.FileUri == null)
                        {
                            return;
                        }

                        if (dataSessionFile.StreamWriter == null)
                        {

                            return;
                        }

                        var result = localConfig.Globals.LState_Success.Clone();

                        using (dataSessionFile.StreamWriter)
                        {
                            if (listType == ListType.AttributeTypes)
                            {
                                var listItems = oitemListViewModel1.ListAdapter.AttributeTypeViewItems;
                                result = attributeTypeFactory.CreateGridDataJsonOfModel(listItems, dataSessionFile.StreamWriter, IsVisible_Apply) ? localConfig.Globals.LState_Success.Clone() : localConfig.Globals.LState_Error.Clone();
                            }
                            else if (listType == ListType.RelationTypes)
                            {
                                var listItems = oitemListViewModel1.ListAdapter.RelationTypeViewItems;
                                result = relationTypeFactory.CreateGridDataJsonOfModel(listItems, dataSessionFile.StreamWriter, IsVisible_Apply) ? localConfig.Globals.LState_Success.Clone() : localConfig.Globals.LState_Error.Clone();
                            }
                            else if (listType == ListType.Instances)
                            {
                                var listItems = oitemListViewModel1.ListAdapter.InstanceViewItems;
                                result = objectFactory.CreateGridDataJsonOfModel(listItems, dataSessionFile.StreamWriter, IsVisible_Apply) ? localConfig.Globals.LState_Success.Clone() : localConfig.Globals.LState_Error.Clone();
                            }
                            else if (listType == ListType.InstanceInstance_Conscious)
                            {
                                var listItems = oitemListViewModel1.ListAdapter.ObjectObjectConsciousViewItems;
                                result = objectRelLeftRightFactory.CreateGridDataJsonOfModel(listItems, dataSessionFile.StreamWriter, IsVisible_Apply) ? localConfig.Globals.LState_Success.Clone() : localConfig.Globals.LState_Error.Clone();

                            }
                            else if (listType == ListType.InstanceInstance_Subconscious)
                            {
                                var listItems = oitemListViewModel1.ListAdapter.ObjectObjectSubconsciousViewItems;
                                result = objectRelRightLeftFactory.CreateGridDataJsonOfModel(listItems, dataSessionFile.StreamWriter, IsVisible_Apply) ? localConfig.Globals.LState_Success.Clone() : localConfig.Globals.LState_Error.Clone();
                            }
                            else if (listType == ListType.InstanceOther_Conscious)
                            {
                                var listItems = oitemListViewModel1.ListAdapter.ObjectOtherConsciousViewItems;
                                result = objectRelLeftRightFactory.CreateGridDataJsonOfModel(listItems, dataSessionFile.StreamWriter, IsVisible_Apply) ? localConfig.Globals.LState_Success.Clone() : localConfig.Globals.LState_Error.Clone();
                            }
                            else if (listType == ListType.InstanceOther_Subconscious)
                            {
                                var listItems = oitemListViewModel1.ListAdapter.ObjectOtherSubconsciousViewItems;
                                result = objectRelRightLeftFactory.CreateGridDataJsonOfModel(listItems, dataSessionFile.StreamWriter, IsVisible_Apply) ? localConfig.Globals.LState_Success.Clone() : localConfig.Globals.LState_Error.Clone();
                            }
                            else if (listType == ListType.InstanceAttribute_Conscious)
                            {
                                var listItems = oitemListViewModel1.ListAdapter.ObjectAttributeViewItems;
                                result = objectAttributeFactory.CreateGridDataJsonOfModel(listItems, dataSessionFile.StreamWriter, IsVisible_Apply) ? localConfig.Globals.LState_Success.Clone() : localConfig.Globals.LState_Error.Clone();
                            }


                        }
                        if (result.GUID == localConfig.Globals.LState_Error.GUID)
                        {

                            return;
                        }

                        webSocketServiceAgent.SendCommand("refreshGrid");
                        LocalStateMachine.ItemListLoaded();
                        IsVisible_Apply = true;


                    }
                }
                catch (Exception)
                {

                    return;
                }
                

            }
            else if (e.PropertyName == "Result_ListLoader")
            {
                if (oitemListViewModel1.ListAdapter.Result_ListLoader.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    omniRelationItems = oitemListViewModel1.ListAdapter.ObjectObjectOmniViewItems;
                    IsVisible_Apply = true;
                    LocalStateMachine.ItemListLoadRequest();
                    oitemListViewModel2.Initialize_ItemList(ListType.InstanceInstance_Omni, new clsObjectRel { ID_Other = oItem.GUID, ID_RelationType = oItem.GUID_Relation, ID_Parent_Object = oItem.GUID_Related });
                }
            }

            
        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
