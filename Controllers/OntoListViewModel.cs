﻿using OItemListController.Notifications;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OItemListController.Controllers
{
    public class OntoListViewModel : ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "loginSuccess", ViewItemType = ViewItemType.Other)]
        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private JqxColumnList columnconfig_Grid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.ColumnConfig)]
        public JqxColumnList ColumnConfig_Grid
        {
            get { return columnconfig_Grid; }
            set
            {
                if (columnconfig_Grid == value) return;

                columnconfig_Grid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_ColumnConfig_Grid);

            }
        }

        private JqxDataSource jqxdatasource_Grid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.DataSource)]
        public JqxDataSource JqxDataSource_Grid
        {
            get { return jqxdatasource_Grid; }
            set
            {
                if (jqxdatasource_Grid == value) return;

                jqxdatasource_Grid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_JqxDataSource_Grid);

            }
        }


        private string text_RowRemove;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.RemoveItem)]
        public string Text_RowRemove
        {
            get { return text_RowRemove; }
            set
            {
                if (text_RowRemove == value) return;

                text_RowRemove = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_RowRemove);

            }
        }

        private clsOntologyItem oitem_RowAdd;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.AddOItem)]
        public clsOntologyItem OItem_RowAdd
        {
            get { return oitem_RowAdd; }
            set
            {
                if (oitem_RowAdd == value) return;

                oitem_RowAdd = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_OItem_RowAdd);

            }
        }

    }
}
