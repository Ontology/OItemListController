﻿using OItemListController.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OItemListController.Controllers
{
    public class OItemRelationListViewModel : OntoMsg_Module.Base.ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true)]
        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.Navigation_IsSuccessful_Login);

            }
        }

        private string url_GridData;
        [ViewModel(Send = true)]
        public string Url_GridData
        {
            get { return url_GridData; }
            set
            {
                if (url_GridData == value) return;
                url_GridData = value;

                RaisePropertyChanged(NotifyChanges.Navigation_Url_GridData);

            }
        }

        private string url_Columns;
        [ViewModel(Send = true)]
        public string Url_Columns
        {
            get { return url_Columns; }
            set
            {
                if (url_Columns == value) return;
                url_Columns = value;

                RaisePropertyChanged(NotifyChanges.Navigation_Url_Columns);

            }
        }
    }
}
