﻿using OItemListController.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OItemListController.Controllers
{
    public class OItemListViewModel : OntoMsg_Module.Base.ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true)]
        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private bool show_Loader;
        [ViewModel(Send = true)]
        public bool Show_Loader
        {
            get { return show_Loader; }
            set
            {
                if (show_Loader == value) return;

                show_Loader = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Show_Loader);

            }
        }

        private bool isenabled_ContextMenu;
        [ViewModel(Send = true)]
        public bool IsEnabled_ContextMenu
        {
            get { return isenabled_ContextMenu; }
            set
            {
                if (isenabled_ContextMenu == value) return;

                isenabled_ContextMenu = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ContextMenu);

            }
        }

        private bool isvisible_ContextMenu;
        [ViewModel(Send = true)]
        public bool IsVisible_ContextMenu
        {
            get { return isvisible_ContextMenu; }
            set
            {
                if (isvisible_ContextMenu == value) return;

                isvisible_ContextMenu = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_ContextMenu);

            }
        }

        private string url_ContextMenu;
        [ViewModel(Send = true)]
        public string Url_ContextMenu
        {
            get { return url_ContextMenu; }
            set
            {
                if (url_ContextMenu == value) return;

                url_ContextMenu = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_ContextMenu);

            }
        }

        private MenuEntry contextmenuentry_ContextMenuEntryChange;
        [ViewModel(Send = true)]
        public MenuEntry ContextMenuEntry_ContextMenuEntryChange
        {
            get { return contextmenuentry_ContextMenuEntryChange; }
            set
            {

                contextmenuentry_ContextMenuEntryChange = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_ContextMenuEntry_ContextMenuEntryChange);

            }
        }

        private List<string> nodeids_Selected;
        [ViewModel(Send = true)]
        public List<string> NodeIds_Selected
        {
            get { return nodeids_Selected; }
            set
            {
                if (nodeids_Selected == value) return;

                nodeids_Selected = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_NodeIds_Selected);

            }
        }

        private JqxColumnList columnconfig_Grid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.ColumnConfig)]
        public JqxColumnList ColumnConfig_Grid
        {
            get { return columnconfig_Grid; }
            set
            {
                if (columnconfig_Grid == value) return;

                columnconfig_Grid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_ColumnConfig_Grid);

            }
        }

        private JqxDataSource jqxdatasource_Grid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.DataSource)]
        public JqxDataSource JqxDataSource_Grid
        {
            get { return jqxdatasource_Grid; }
            set
            {
                if (jqxdatasource_Grid == value) return;

                jqxdatasource_Grid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_JqxDataSource_Grid);

            }
        }

        private List<string> nodeids_Applied;
        [ViewModel(Send = true)]
        public List<string> NodeIds_Applied
        {
            get { return nodeids_Applied; }
            set
            {
                if (nodeids_Applied == value) return;

                nodeids_Applied = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_NodeIds_Applied);

            }
        }

        private bool isenabled_Apply = true;
        [ViewModel(Send = true, ViewItemId = "applyItem", ViewItemType = ViewItemType.Enable, ViewItemClass = ViewItemClass.Button)]
        public bool IsEnabled_Apply
        {
            get { return isenabled_Apply; }
            set
            {

                isenabled_Apply = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Apply);

            }
        }

        private bool isvisible_Apply = true;
        [ViewModel(Send = true)]
        public bool IsVisible_Apply
        {
            get { return isvisible_Apply; }
            set
            {
                if (isvisible_Apply == value) return;

                isvisible_Apply = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_Apply);

            }
        }

        private string url_NewItem;
        [ViewModel(Send = true)]
        public string Url_NewItem
        {
            get { return url_NewItem; }
            set
            {
                if (url_NewItem == value) return;

                url_NewItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_NewItem);

            }
        }

        private string text_View;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewText", ViewItemType = ViewItemType.Other)]
        public string Text_View
        {
            get { return text_View; }
            set
            {
                if (text_View == value) return;

                text_View = value;

                RaisePropertyChanged(NotifyChanges.View_Text_View);

            }
        }

        private string datatext_AttributeTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "attributeTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_AttributeTypeId
        {
            get { return datatext_AttributeTypeId; }
            set
            {
                if (datatext_AttributeTypeId == value) return;

                datatext_AttributeTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_AttributeTypeId);

            }
        }

        private string datatext_RelationTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "relationTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_RelationTypeId
        {
            get { return datatext_RelationTypeId; }
            set
            {
                if (datatext_RelationTypeId == value) return;

                datatext_RelationTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_RelationTypeId);

            }
        }

        private string datatext_ObjectId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "objectId", ViewItemType = ViewItemType.Other)]
        public string DataText_ObjectId
        {
            get { return datatext_ObjectId; }
            set
            {
                if (datatext_ObjectId == value) return;

                datatext_ObjectId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ObjectId);

            }
        }

        private string datatext_ClassId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "classId", ViewItemType = ViewItemType.Other)]
        public string DataText_ClassId
        {
            get { return datatext_ClassId; }
            set
            {
                if (datatext_ClassId == value) return;

                datatext_ClassId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ClassId);

            }
        }

        private bool istoggled_Listen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListen", ViewItemType = ViewItemType.Other)]
        public bool IsToggled_Listen
        {
            get { return istoggled_Listen; }
            set
            {

                istoggled_Listen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_Listen);

            }
        }

        private JqxCellItem cellitem_CellDoubleClicked;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.DoubleClick)]
        public JqxCellItem CellItem_CellDoubleClicked
        {
            get { return cellitem_CellDoubleClicked; }
            set
            {
                if (cellitem_CellDoubleClicked == value) return;

                cellitem_CellDoubleClicked = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_CellItem_CellDoubleClicked);

            }
        }

        private JqxCellItem cellitem_CellSelected;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.SelectedIndex)]
        public JqxCellItem CellItem_CellSelected
        {
            get { return cellitem_CellSelected; }
            set
            {
                if (cellitem_CellSelected == value) return;

                cellitem_CellSelected = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_CellItem_CellSelected);

            }
        }

        private bool isenabled_CopyGuid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "cpyGuid", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_CopyGuid
        {
            get { return isenabled_CopyGuid; }
            set
            {
                if (isenabled_CopyGuid == value) return;

                isenabled_CopyGuid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_CopyGuid);

            }
        }

        private string label_CopyGuid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "cpyGuid", ViewItemType = ViewItemType.Content)]
        public string Label_CopyGuid
        {
            get { return label_CopyGuid; }
            set
            {
                if (label_CopyGuid == value) return;

                label_CopyGuid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_CopyGuid);

            }
        }

        private string text_EditMessageOrderId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "editMessageOrderId", ViewItemType = ViewItemType.Other)]
        public string Text_EditMessageOrderId
        {
            get { return text_EditMessageOrderId; }
            set
            {
                if (text_EditMessageOrderId == value) return;

                text_EditMessageOrderId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_EditMessageOrderId);

            }
        }

        private string text_EditMessageName;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "editMessageName", ViewItemType = ViewItemType.Other)]
        public string Text_EditMessageName
        {
            get { return text_EditMessageName; }
            set
            {
                if (text_EditMessageName == value) return;

                text_EditMessageName = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_EditMessageName);

            }
        }

        private bool isenabled_SaveOrderId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "saveOrderId", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_SaveOrderId
        {
            get { return isenabled_SaveOrderId; }
            set
            {
                if (isenabled_SaveOrderId == value) return;

                isenabled_SaveOrderId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_SaveOrderId);

            }
        }

        private string label_SaveOrderId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "saveOrderId", ViewItemType = ViewItemType.Content)]
        public string Label_SaveOrderId
        {
            get { return label_SaveOrderId; }
            set
            {
                if (label_SaveOrderId == value) return;

                label_SaveOrderId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_SaveOrderId);

            }
        }

        private bool isenabled_SaveName;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "saveName", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_SaveName
        {
            get { return isenabled_SaveName; }
            set
            {
                if (isenabled_SaveName == value) return;

                isenabled_SaveName = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_SaveName);

            }
        }

        private string label_SaveName;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "saveName", ViewItemType = ViewItemType.Content)]
        public string Label_SaveName
        {
            get { return label_SaveName; }
            set
            {
                if (label_SaveName == value) return;

                label_SaveName = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_SaveName);

            }
        }

        private int int_OrderIdValue;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.NumericInput, ViewItemId = "inpValueEditOrderId", ViewItemType = ViewItemType.Content)]
        public int Int_OrderIdValue
        {
            get { return int_OrderIdValue; }
            set
            {
                if (int_OrderIdValue == value) return;

                int_OrderIdValue = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Int_OrderIdValue);

            }
        }

        private string label_ValueOrderId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblValueOrderId", ViewItemType = ViewItemType.Content)]
        public string Label_ValueOrderId
        {
            get { return label_ValueOrderId; }
            set
            {
                if (label_ValueOrderId == value) return;

                label_ValueOrderId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_ValueOrderId);

            }
        }

        private string datatext_NameValue;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpValueEditName", ViewItemType = ViewItemType.Content)]
        public string DataText_NameValue
        {
            get { return datatext_NameValue; }
            set
            {
                if (datatext_NameValue == value) return;

                datatext_NameValue = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_NameValue);

            }
        }

        private string label_ValueName;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblValueName", ViewItemType = ViewItemType.Content)]
        public string Label_ValueName
        {
            get { return label_ValueName; }
            set
            {
                if (label_ValueName == value) return;

                label_ValueName = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_ValueName);

            }
        }

        private string text_Guid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "txtGuid", ViewItemType = ViewItemType.Content)]
        public string Text_Guid
        {
            get { return text_Guid; }
            set
            {
                if (text_Guid == value) return;

                text_Guid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_Guid);

            }
        }

        private string label_Guid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblGuid", ViewItemType = ViewItemType.Content)]
        public string Label_Guid
        {
            get { return label_Guid; }
            set
            {
                if (label_Guid == value) return;

                label_Guid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Guid);

            }
        }

        private JqxCellItem cellitem_ChangedItem;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.Change)]
        public JqxCellItem CellItem_ChangedItem
        {
            get { return cellitem_ChangedItem; }
            set
            {
                if (cellitem_ChangedItem == value) return;

                cellitem_ChangedItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_CellItem_ChangedItem);

            }
        }

        private string text_CellGuid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpCellGuid", ViewItemType = ViewItemType.Content)]
		public string Text_CellGuid
        {
            get { return text_CellGuid; }
            set
            {
                if (text_CellGuid == value) return;

                text_CellGuid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_CellGuid);

            }
        }

        private bool isenabled_AddToClipboard;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "addToClipboard", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_AddToClipboard
        {
            get { return isenabled_AddToClipboard; }
            set
            {

                isenabled_AddToClipboard = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_AddToClipboard);

            }
        }

        private string text_HideWaitViewItemId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "hideWaitViewItemId", ViewItemType = ViewItemType.Other)]
		public string Text_HideWaitViewItemId
        {
            get { return text_HideWaitViewItemId; }
            set
            {
                if (text_HideWaitViewItemId == value) return;

                text_HideWaitViewItemId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_HideWaitViewItemId);

            }
        }

        private string datatext_Filter;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.Filter)]
		public string DataText_Filter
        {
            get { return datatext_Filter; }
            set
            {
                if (datatext_Filter == value) return;

                datatext_Filter = value;

                RaisePropertyChanged(NotifyChanges.View_DataText_Filter);

            }
        }

        private string text_RowRemove;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.RemoveItem)]
		public string Text_RowRemove
        {
            get { return text_RowRemove; }
            set
            {

                text_RowRemove = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_RowRemove);

            }
        }
    }
}
