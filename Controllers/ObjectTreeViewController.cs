﻿using OItemListController.Factories;
using OItemListController.Services;
using OItemListController.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace OItemListController.Controllers
{
    public class ObjectTreeViewController : ObjectTreeViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private ServiceAgent_ElasticObjectTree serviceAgent_Elastic;
        
        private ObjectTreeFactory treeFactory;

        private clsLocalConfig localConfig;

        private clsOntologyItem selectedClass;
        private clsOntologyItem selectedRelationType;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public ObjectTreeViewController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();

            PropertyChanged += ObjectTreeViewController_PropertyChanged;
        }

        private void ObjectTreeViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);

        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgent_Elastic = new ServiceAgent_ElasticObjectTree(localConfig);
            serviceAgent_Elastic.PropertyChanged += ServiceAgent_Elastic_PropertyChanged;
            treeFactory = new ObjectTreeFactory();
            treeFactory.PropertyChanged += TreeFactory_PropertyChanged;

            

        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
            
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.SelectedClassNode,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.AppliedObjects,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            IsToggled_Listen = true;

            var result = serviceAgent_Elastic.GetRelationTypes();


            

            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void TreeFactory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ObjectTreeFactory.ResultTreeNodes))
            {
                if (treeFactory.ResultTreeNodes.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    SetTreeViewConfig(treeFactory.SessionFile);
                }
            }
        }

        private void ServiceAgent_Elastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ServiceAgent_ElasticObjectTree.ResultRelationTypes))
            {
                if (serviceAgent_Elastic.ResultRelationTypes.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    KendoDropDownConfig_RelationTypes = new OntoWebCore.Models.KendoDropDownConfig
                    {
                        optionLabel = "Relation-Type",
                        dataSource = serviceAgent_Elastic.RelationTypes.OrderBy(relType => relType.Name).Select(relType => new OntoWebCore.Models.KendoDropDownItem
                        {
                            Text = relType.Name,
                            Value = relType.GUID
                        }).ToList()
                    };
                }
            }
            else if (e.PropertyName == nameof(ServiceAgent_ElasticObjectTree.RelationTypesFiltered))
            {
                KendoDropDownConfig_RelationTypes = new OntoWebCore.Models.KendoDropDownConfig
                {
                    optionLabel = "Relation-Type",
                    dataSource = serviceAgent_Elastic.RelationTypesFiltered.OrderBy(relType => relType.Name).Select(relType => new OntoWebCore.Models.KendoDropDownItem
                    {
                        Text = relType.Name,
                        Value = relType.GUID
                    }).ToList()
                };
            }
            else if (e.PropertyName == nameof(ServiceAgent_ElasticObjectTree.ResultFlatItems))
            {
                if (serviceAgent_Elastic.ResultFlatItems.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    
                    var sessionFile = webSocketServiceAgent.RequestWriteStream(Guid.NewGuid().ToString() + ".json");

                    var treeNodes = serviceAgent_Elastic.FlatItems.OrderBy(flatItem => flatItem.Name).Select(flatItem => new KendoTreeNode
                    {
                        NodeId = flatItem.GUID,
                        NodeName = flatItem.Name
                    });
                    using (sessionFile.StreamWriter)
                    {
                        using (var jsonWriter = new Newtonsoft.Json.JsonTextWriter(sessionFile.StreamWriter))
                        {
                            
                            jsonWriter.WriteRaw(Newtonsoft.Json.JsonConvert.SerializeObject(treeNodes));
                            
                            
                        }
                            
                    }

                    SetTreeViewConfig(sessionFile);
                }
            }
            else if (e.PropertyName == nameof(ServiceAgent_ElasticObjectTree.ResultRelationType))
            {
                if (serviceAgent_Elastic.ResultRelationType.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    selectedRelationType = serviceAgent_Elastic.RelationType;
                    DataText_DropDownItemId = selectedRelationType.GUID;
                    RaisePropertyChanged(nameof(DataText_DropDownItemId));
                    LoadTreeData();
                }
            }
            else if (e.PropertyName == nameof(ServiceAgent_ElasticObjectTree.ResultObjectTree))
            {
                if (serviceAgent_Elastic.ResultObjectTree.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var sessionFile = webSocketServiceAgent.RequestWriteStream(Guid.NewGuid().ToString() + ".json");

                    if (sessionFile == null) return;

                    var result = treeFactory.WriteTreeNodes(serviceAgent_Elastic.Objects, serviceAgent_Elastic.ObjectTree, sessionFile);
                }
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;


        }

       

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == "addSubItem")
                {

                    if (KendoTreeNode_TreeNodeId == null && localConfig.Globals.is_GUID(KendoTreeNode_TreeNodeId.NodeId)) return;
                    if (selectedClass == null) return;

                    var view = ModuleDataExchanger.GetViewById("523c22bb4ea44b8da38259218e5c29fb");

                    var navigation = OntoMsg_Module.ModuleDataExchanger.CurrentWebApp.EntryUrl + (OntoMsg_Module.ModuleDataExchanger.CurrentWebApp.EntryUrl.EndsWith("/") ? "" : "/") + view.NameCommandLineRun;

                    navigation += "?Class=" + selectedClass.GUID + "&Sender=" + webSocketServiceAgent.EndpointId;
                    Url_AddUrl = navigation;

                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "applyItems")
                {
                    var oItems = Stringlist_CheckedItems.Select(checkedId => { return serviceAgent_Elastic.GetOItem(checkedId, localConfig.Globals.Type_Object); }).Where(checkedId => checkedId != null).ToList();
                    var interModMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.AppliedObjects,
                        OItems = oItems
                    };

                    webSocketServiceAgent.SendInterModMessage(interModMessage);
                }

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {
                    if (changedItem.ViewItemId == "dropdRelationType")
                    {
                        // Read tree-data by guid of relationtype
                        if (!string.IsNullOrEmpty(changedItem.ViewItemValue.ToString()))
                        {
                            var relationType = serviceAgent_Elastic.GetOItem(changedItem.ViewItemValue.ToString(), localConfig.Globals.Type_RelationType);
                            if (selectedRelationType == null || relationType.GUID != selectedRelationType.GUID)
                            {
                                selectedRelationType = relationType;
                                LoadTreeData();
                            }
                                
                            
                        }
                        else
                        {
                            selectedRelationType = null;
                            LoadTreeData();
                        }

                        
                        
                    }
                    else if (changedItem.ViewItemClass == "TreeNode" && changedItem.ViewItemType == "SelectedIndex")
                    {
                        if (!string.IsNullOrEmpty(changedItem.ViewItemValue.ToString()) && localConfig.Globals.is_GUID(changedItem.ViewItemValue.ToString()))
                        {
                            

                            

                            var node = Newtonsoft.Json.JsonConvert.DeserializeObject<KendoTreeNode>(changedItem.ViewItemValue.ToString());

                            var oItem = serviceAgent_Elastic.GetOItem(node.NodeId, localConfig.Globals.Type_Object);

                            if (oItem == null) return;

                            KendoTreeNode_TreeNodeId = node;

                            var interModMessage = new InterServiceMessage
                            {
                                ChannelId = Channels.ParameterList,
                                OItems = new List<clsOntologyItem> { oItem }
                            };

                            webSocketServiceAgent.SendInterModMessage(interModMessage);
                        }
                        
                        
                    }
                    else if (changedItem.ViewItemClass == "TreeNode" && changedItem.ViewItemType == "checkedItems")
                    {
                        Stringlist_CheckedItems = changedItem.ViewItemValue != null ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(changedItem.ViewItemValue.ToString()) : null;
                    }
                });
            }



        }

        private void LoadTreeData()
        {
            SessionFile sessionFile = null;
            if (selectedRelationType == null && selectedClass == null)
            {
                sessionFile = webSocketServiceAgent.RequestWriteStream(Guid.NewGuid().ToString() + ".json");
                if (sessionFile == null) return;

                using (sessionFile.StreamWriter)
                {
                    using (var jsonWriter = new Newtonsoft.Json.JsonTextWriter(sessionFile.StreamWriter))
                    {
                        jsonWriter.WriteStartObject();
                        jsonWriter.WritePropertyName("data");
                        jsonWriter.WriteStartArray();
                        jsonWriter.WriteEndArray();
                        jsonWriter.WriteEndObject();
                    }
                }

                SetTreeViewConfig(sessionFile);
            }
            else if (selectedRelationType == null && selectedClass != null)
            {
                var result = serviceAgent_Elastic.GetFlatItems(selectedClass);
            }
            else if (selectedClass != null && selectedRelationType != null)
            {
                var result = serviceAgent_Elastic.GetObjectTree(selectedClass, selectedRelationType);
            }

            

            

        }

        private void SetTreeViewConfig(SessionFile sessionFile)
        {
            KendoTreeViewConfig_TreeViewConfig = new KendoTreeViewConfig
            {
                dataTextField = nameof(KendoTreeNode.NodeName),
                dataSource = new KendoHierarchicalDataSource
                {
                    transport = new KendoTransport
                    {
                        read = new KendoTransportRead
                        {
                            url = sessionFile.FileUri.AbsoluteUri,
                            dataType = "json"
                        }
                    },
                    schema = new KendoHierarchicalSchema
                    {
                        model = new KendoHierarchicalModel
                        {
                            id = nameof(KendoTreeNode.NodeId),
                            children = nameof(KendoTreeNode.SubNodes),
                            hasChildren = nameof(KendoTreeNode.HasChildren)
                        }
                    }
                }
            };
        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {

            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;

            if (message.ChannelId == Channels.ReceiverOfDedicatedSender)
            {
            }
            else if (message.ChannelId == Channels.SelectedClassNode)
            {
                var oItem = message.OItems.FirstOrDefault();

                if (oItem == null) return;

                selectedRelationType = null;
                oItem = serviceAgent_Elastic.GetOItem(oItem.GUID, localConfig.Globals.Type_Class);

                selectedClass = oItem;

                var result = serviceAgent_Elastic.GetRelationType(selectedClass);

                
            }
            else if (message.ChannelId == Channels.AppliedObjects)
            {
                var objects = message.OItems;
                if (selectedClass == null) return;
                
                if (KendoTreeNode_TreeNodeId == null || !localConfig.Globals.is_GUID(KendoTreeNode_TreeNodeId.NodeId)) return;

                var selectedObject = serviceAgent_Elastic.GetOItem(KendoTreeNode_TreeNodeId.NodeId, localConfig.Globals.Type_Object);
                if (selectedObject == null) return;

                if (selectedRelationType == null) return;

                if (!objects.All(obj => obj.GUID_Parent == selectedObject.GUID_Parent)) return;

                var result = serviceAgent_Elastic.SaveSubItems(selectedObject, objects, selectedRelationType, IsChecked_TakeNextId);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var treeNodes = new List<KendoTreeNode> { new KendoTreeNode
                    {
                        NodeId = selectedObject.GUID,
                        NodeName = selectedObject.Name,
                        NodeUId = KendoTreeNode_TreeNodeId.NodeUId,
                        SubNodes = objects.Select(obj => new KendoTreeNode
                        {
                            NodeId = obj.GUID,
                            NodeName = obj.Name
                        }).ToList()
                    } };

                    KendoTreeNodes_NewNodes = treeNodes;
                }
            }

        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
