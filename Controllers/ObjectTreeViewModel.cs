﻿using OItemListController.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OItemListController.Controllers
{
    public class ObjectTreeViewModel : ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "loginSuccess", ViewItemType = ViewItemType.Other)]
		public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private bool istoggled_Listen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListen", ViewItemType = ViewItemType.Other)]
		public bool IsToggled_Listen
        {
            get { return istoggled_Listen; }
            set
            {
                if (istoggled_Listen == value) return;

                istoggled_Listen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_Listen);

            }
        }

        private string resource_TreeData;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Url, ViewItemId = "treeData", ViewItemType = ViewItemType.JsonUrl)]
		public string Resource_TreeData
        {
            get { return resource_TreeData; }
            set
            {
                if (resource_TreeData == value) return;

                resource_TreeData = value;

                RaisePropertyChanged(NotifyChanges.ViewController_Resource_TreeData);

            }
        }

        private KendoDropDownConfig kendodropdownconfig_RelationTypes;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.KendoDropDownList, ViewItemId = "dropdRelationType", ViewItemType = ViewItemType.Content)]
        public KendoDropDownConfig KendoDropDownConfig_RelationTypes
        {
            get { return kendodropdownconfig_RelationTypes; }
            set
            {
                if (kendodropdownconfig_RelationTypes == value) return;

                kendodropdownconfig_RelationTypes = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_KendoDropDownConfig_RelationTypes);

            }
        }

        private string datatext_DropDownItemId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.KendoDropDownList, ViewItemId = "dropdRelationType", ViewItemType = ViewItemType.SelectedIndex)]
		public string DataText_DropDownItemId
        {
            get { return datatext_DropDownItemId; }
            set
            {
                if (datatext_DropDownItemId == value) return;

                datatext_DropDownItemId = value;


            }
        }

        private KendoTreeViewConfig kendotreeviewconfig_TreeViewConfig;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "objectTreeView", ViewItemType = ViewItemType.Other)]
		public KendoTreeViewConfig KendoTreeViewConfig_TreeViewConfig
        {
            get { return kendotreeviewconfig_TreeViewConfig; }
            set
            {
                if (kendotreeviewconfig_TreeViewConfig == value) return;

                kendotreeviewconfig_TreeViewConfig = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_KendoTreeViewConfig_TreeViewConfig);

            }
        }

        private KendoTreeNode kendotreenode_TreeNodeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.TreeNode, ViewItemId = "objectTreeView", ViewItemType = ViewItemType.SelectedIndex)]
		public KendoTreeNode KendoTreeNode_TreeNodeId
        {
            get { return kendotreenode_TreeNodeId; }
            set
            {
                if (kendotreenode_TreeNodeId == value) return;

                kendotreenode_TreeNodeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_KendoTreeNode_TreeNodeId);

            }
        }


        private bool ischecked_TakeNextId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "isToggledNextId", ViewItemType = ViewItemType.Checked)]
		public bool IsChecked_TakeNextId
        {
            get { return ischecked_TakeNextId; }
            set
            {
                if (ischecked_TakeNextId == value) return;

                ischecked_TakeNextId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsChecked_TakeNextId);

            }
        }

        private string url_AddUrl;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Url, ViewItemId = "addUrl", ViewItemType = ViewItemType.Other)]
		public string Url_AddUrl
        {
            get { return url_AddUrl; }
            set
            {

                url_AddUrl = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_AddUrl);

            }
        }

        private List<KendoTreeNode> kendotreenodes_NewNodes;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "kendoTreeNodesNew", ViewItemType = ViewItemType.Other)]
		public List<KendoTreeNode> KendoTreeNodes_NewNodes
        {
            get { return kendotreenodes_NewNodes; }
            set
            {
                if (kendotreenodes_NewNodes == value) return;

                kendotreenodes_NewNodes = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_KendoTreeNodes_NewNodes);

            }
        }

        private bool ischecked_ShowCheckboxes;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "isToggledCheck", ViewItemType = ViewItemType.Checked)]
		public bool IsChecked_ShowCheckboxes
        {
            get { return ischecked_ShowCheckboxes; }
            set
            {
                if (ischecked_ShowCheckboxes == value) return;

                ischecked_ShowCheckboxes = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsChecked_ShowCheckboxes);

            }
        }

        private List<string> stringlist_CheckedItems;
        public List<string> Stringlist_CheckedItems
        {
            get { return stringlist_CheckedItems; }
            set
            {
                if (stringlist_CheckedItems == value) return;

                stringlist_CheckedItems = value;



            }
        }
    }
}
