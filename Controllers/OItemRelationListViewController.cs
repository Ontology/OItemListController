﻿using OItemListController.Factories;
using OItemListController.Models;
using OItemListController.Services;
using OItemListController.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.Services;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace OItemListController.Controllers
{
    public class OItemRelationListViewController : OItemRelationListViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private ViewModelOItemList oitemListViewModel;

        private DataGridJsonFactory<InstanceViewItem> objectFactory = new DataGridJsonFactory<InstanceViewItem>();

        private TranslationController translationController = new TranslationController();

        private ServiceAgent_ElasticOItemList serviceAgent_Elastic;


        private clsLocalConfig localConfig;

        private string fileNameData;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public OItemRelationListViewController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            PropertyChanged += OItemListViewController_PropertyChanged;
        }

        private void OItemListViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            webSocketServiceAgent.SendPropertyChange(e.PropertyName);
        }

        
        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            oitemListViewModel = new ViewModelOItemList(localConfig.Globals);
            oitemListViewModel.PropertyChanged += ViewModelWinFormsOItemList_PropertyChanged;

        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();

            oitemListViewModel = null;
            objectFactory = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

        }

        private void StateMachine_loginSucceded()
        {
            

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.SelectedClassNode,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            serviceAgent_Elastic = new ServiceAgent_ElasticOItemList(localConfig);
            var fileName = Guid.NewGuid().ToString() + ".json";
            fileNameData = Guid.NewGuid().ToString() + ".json";

            var sessionFile = webSocketServiceAgent.RequestWriteStream(fileNameData);
            if (sessionFile.FileUri == null)
            {
                return;
            }

            if (sessionFile.StreamWriter == null)
            {

                return;
            }

            var result = localConfig.Globals.LState_Success.Clone();
            using (sessionFile.StreamWriter)
            {
                if (!objectFactory.CreateEmptyData(sessionFile.StreamWriter))
                {
                    result = localConfig.Globals.LState_Error.Clone();
                }

            }
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {

                return;
            }



            Url_Columns = sessionFile.FileUri.AbsoluteUri;
            

            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;
            
        }

        

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {
                

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == "SelectedObjectRow")
                {
                    var objectId = webSocketServiceAgent.Request["IdInstance"].ToString();
                    if (string.IsNullOrEmpty(objectId) && !localConfig.Globals.is_GUID(objectId)) return;

                    var oItem = serviceAgent_Elastic.GetOItem(objectId, localConfig.Globals.Type_Object);

                    if (oItem == null) return;

                    var selectedObjectRequest = new InterServiceMessage
                    {
                        ChannelId = Channels.ParameterList,
                        SenderId = webSocketServiceAgent.EndpointId,
                        OItems = new List<clsOntologyItem> { oItem }
                    };

                    webSocketServiceAgent.SendInterModMessage(selectedObjectRequest);

                }
                


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {
                
            }


        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.SelectedClassNode)
            {
                var oItem = message.OItems.LastOrDefault();

                if (oItem == null) return;
                oitemListViewModel.Initialize_ItemList(ListType.Instances, new clsOntologyItem { GUID_Parent = oItem.GUID }, true, DestinationType.None);
                
            }
        }

        private void ViewModelWinFormsOItemList_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Result_ListLoader")
            {
                if (oitemListViewModel.ListAdapter.Result_ListLoader.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var listItems = oitemListViewModel.ListAdapter.InstanceViewItems;
                    fileNameData = Guid.NewGuid().ToString() + ".json";

                    var sessionFile = webSocketServiceAgent.RequestWriteStream(fileNameData);
                    if (sessionFile.FileUri == null)
                    {
                        return;
                    }

                    if (sessionFile.StreamWriter == null)
                    {

                        return;
                    }

                    var result = localConfig.Globals.LState_Success.Clone();

                    using (sessionFile.StreamWriter)
                    {
                        result = objectFactory.CreateGridDataJsonOfModel(listItems, sessionFile.StreamWriter, true) ? localConfig.Globals.LState_Success.Clone() : localConfig.Globals.LState_Error.Clone();

                    }
                    if (result.GUID == localConfig.Globals.LState_Error.GUID)
                    {

                        return;
                    }

                    Url_GridData = sessionFile.FileUri.AbsoluteUri;
                }
            }
        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
