﻿using OItemListController.Notifications;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OItemListController.Models
{
    public class RelationTypeViewItem : NotifyPropertyChange
    {
        private clsOntologyItem proxyItem;

        public string IdRelationType
        {
            get { return proxyItem.GUID; }
            set
            {
                proxyItem.GUID = value;
                RaisePropertyChanged(NotifyChanges.OItemList_RelationType_IdRelationType);
            }
        }


        [DataViewColumn(IsVisible = true, DisplayOrder = 0)]
        public string NameRelationType
        {
            get { return proxyItem.Name; }
            set
            {
                proxyItem.Name = value;
                RaisePropertyChanged(NotifyChanges.OItemList_RelationType_NameRelationType);
            }
        }

        public clsOntologyItem SaveItem(Globals globals)
        {
            var dbConnectorSave = new OntologyModDBConnector(globals);

            var result = dbConnectorSave.SaveRelationTypes(new List<clsOntologyItem> { proxyItem });

            return result;
        }

        [DataViewColumn(IsVisible = false, DisplayOrder = 1, CellType = CellType.String, Width = 250)]
        public string IdItem
        {
            get
            {
                return proxyItem != null ? proxyItem.GUID : null;
            }
        }

        public RelationTypeViewItem(clsOntologyItem proxyItem)
        {
            this.proxyItem = proxyItem;
        }
    }
}
