﻿using OItemListController.Notifications;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OItemListController.Models
{
    public class ClassRelationOmniViewItem : ClassRelationViewItem
    {


        [DataViewColumn(IsVisible = true,
                    DisplayOrder = 0, CellType = CellType.String)]
        public string NameClassLeft
        {
            get { return proxyItem.Name_Class_Left; }
            set
            {
                proxyItem.Name_Class_Left = value;
                RaisePropertyChanged(NotifyChanges.OItemList_ClassRel_NameClassLeft);
            }
        }

        [DataViewColumn(IsVisible = true,
                    DisplayOrder = 2, CellType = CellType.String)]
        public string NameClassRight
        {
            get { return proxyItem.Name_Class_Right; }
            set
            {
                proxyItem.Name_Class_Right = value;
                RaisePropertyChanged(NotifyChanges.OItemList_ClassRel_NameClassRight);
            }
        }

        [DataViewColumn(IsVisible = true,
            DisplayOrder = 1, CellType = CellType.String)]
        public string NameRelationType
        {
            get { return proxyItem.Name_RelationType; }
            set
            {
                proxyItem.Name_Class_Right = value;
                RaisePropertyChanged(NotifyChanges.OItemList_ClassRel_NameRelationType);
            }
        }

        [DataViewColumn(IsVisible = true,
            DisplayOrder = 2, CellType = CellType.String)]
        public string Ontology
        {
            get { return proxyItem.Ontology; }
            set
            {
                proxyItem.Ontology = value;
                RaisePropertyChanged(NotifyChanges.OItemList_ClassRel_Ontology);
            }
        }

        [DataViewColumn(IsVisible = true,
            DisplayOrder = 3, CellType = CellType.Integer)]
        public long Min_Forw
        {
            get { return proxyItem.Min_Forw ?? 0; }
            set
            {
                proxyItem.Min_Forw = value;
                RaisePropertyChanged(NotifyChanges.OItemList_ClassRel_Min_Forw);
            }
        }

        [DataViewColumn(IsVisible = true,
            DisplayOrder = 4, CellType = CellType.Integer)]
        public long Max_Forw
        {
            get { return proxyItem.Max_Forw ?? 0; }
            set
            {
                proxyItem.Max_Forw = value;
                RaisePropertyChanged(NotifyChanges.OItemList_ClassRel_Max_Forw);
            }
        }

        [DataViewColumn(IsVisible = true,
            DisplayOrder = 5, CellType = CellType.Integer)]
        public long Max_Backw
        {
            get { return proxyItem.Max_Backw ?? 0; }
            set
            {
                proxyItem.Max_Forw = value;
                RaisePropertyChanged(NotifyChanges.OItemList_ClassRel_Max_Backw);
            }
        }

        [DataViewColumn(IsVisible = false, DisplayOrder = 1, CellType = CellType.String, Width = 250)]
        public string IdItem
        {
            get
            {
                return proxyItem != null ? proxyItem.ID_Class_Left + proxyItem.ID_Class_Right + proxyItem.ID_RelationType : null;
            }
        }

        public ClassRelationOmniViewItem(clsClassRel proxyItem) : base(proxyItem)
        {
            
        }
    }
}
