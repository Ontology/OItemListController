﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OItemListController.Models
{
    public class AppliedItem
    {
        public ListType ListType { get; set; }
        public object ListItem { get; set; }
    }
}
