﻿using OItemListController.Notifications;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OItemListController.Models
{
    public class InstanceViewItem : NotifyPropertyChange
    {
        private clsOntologyItem proxyItem;

        [DataViewColumn(IsVisible = false)]
        public string IdInstance
        {
            get { return proxyItem.GUID; }
            set
            {
                proxyItem.GUID = value;
                RaisePropertyChanged(NotifyChanges.OItemList_Instance_IdInstance);
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 0, CellType = CellType.String)]
        public string NameInstance
        {
            get { return proxyItem.Name; }
            set
            {
                proxyItem.Name = value;
                RaisePropertyChanged(NotifyChanges.OItemList_Instance_NameInstance);
            }
        }

        public string IdClass
        {
            get { return proxyItem.GUID_Parent; }
            set
            {
                proxyItem.GUID_Parent = value;
                RaisePropertyChanged(NotifyChanges.OItemList_Instance_IdClass);
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 1, CellType = CellType.String, Width = 250)]
        public string NameClass
        {
            get { return proxyItem.Name_Parent; }
            set
            {
                proxyItem.Name_Parent = value;
                RaisePropertyChanged(NotifyChanges.OItemList_Instance_NameClass);
            }
        }

        public clsOntologyItem SaveItem(Globals globals)
        {
            var dbConnectorSave = new OntologyModDBConnector(globals);

            var result = dbConnectorSave.SaveClass(new List<clsOntologyItem> { proxyItem });

            return result;
        }

        [DataViewColumn(IsVisible = false, DisplayOrder = 1, CellType = CellType.String, Width = 250)]
        public string IdItem
        {
            get
            {
                return proxyItem != null ? proxyItem.GUID : null;
            }
        }

        public InstanceViewItem(clsOntologyItem proxyItem, clsOntologyItem classItem)
        {
            this.proxyItem = proxyItem;
            this.proxyItem.Name_Parent = classItem.Name;
        }
    }

   
}
