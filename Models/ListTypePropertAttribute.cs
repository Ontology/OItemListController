﻿using OntoMsg_Module.Attributes;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OItemListController.Models
{
    public class ListTypePropertAttribute
    {
        public PropertyInfo PopertyItem { get; set; }
        public DataViewColumnAttribute ColumnAttribute { get; set; }
    }
}
