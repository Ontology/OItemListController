﻿using OItemListController.Notifications;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OItemListController.Models
{
    public class AttributeTypeViewItem : NotifyPropertyChange
    {
        private clsOntologyItem proxyItem;

        [DataViewColumn(IsVisible = false)]
        public string IdAttributeType
        {
            get { return proxyItem.GUID; }
            set
            {
                proxyItem.GUID = value;
                RaisePropertyChanged(NotifyChanges.OItemList_AttributeType_IdAttributeType);
            }
        }

        [DataViewColumn(IsVisible =true, DisplayOrder = 0, CellType = CellType.String)]
        public string NameAttributeType
        {
            get { return proxyItem.Name; }
            set
            {
                proxyItem.Name = value;
                RaisePropertyChanged(NotifyChanges.OItemList_AttributeType_NameAttributeType);
            }
        }

        public string IdDataType
        {
            get { return proxyItem.GUID_Parent; }
            set
            {
                proxyItem.GUID_Parent = value;
                RaisePropertyChanged(NotifyChanges.OItemList_AttributeType_IdDataType);
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 1, CellType = CellType.String, Width = 250)]
        public string NameDataType
        {
            get { return proxyItem.Name_Parent; }
            set
            {
                proxyItem.Name_Parent = value;
                RaisePropertyChanged(NotifyChanges.OItemList_AttributeType_NameDataType);
            }
        }

        [DataViewColumn(IsVisible = false, DisplayOrder = 1, CellType = CellType.String, Width = 250)]
        public string IdItem
        {
            get
            {
                return proxyItem != null ? proxyItem.GUID : null;
            }
        }

        public clsOntologyItem SaveItem(Globals globals)
        {
            var dbConnectorSave = new OntologyModDBConnector(globals);

            var result = dbConnectorSave.SaveAttributeTypes(new List<clsOntologyItem> { proxyItem });

            return result;
        }

        
        public AttributeTypeViewItem(clsOntologyItem proxyItem, clsOntologyItem dataTypeItem)
        {
            this.proxyItem = proxyItem;
            this.proxyItem.Name_Parent = dataTypeItem.Name;
        }
    }
}
