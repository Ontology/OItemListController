﻿using OItemListController.Notifications;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OItemListController.Models
{
    public class ObjectAttributeViewItem : NotifyPropertyChange
    {
        private clsObjectAtt proxyItem;

        [DataViewColumn(IsVisible = false)]
        public string IdAttribute
        {
            get { return proxyItem.ID_Attribute; }
            set
            {
                proxyItem.ID_Attribute = value;
                RaisePropertyChanged(NotifyChanges.OItemList_Attribute_IdAttribute);
            }
        }

        public string IdAttributeType
        {
            get { return proxyItem.ID_AttributeType; }
            set
            {
                proxyItem.ID_AttributeType = value;
                RaisePropertyChanged(NotifyChanges.OItemList_Attribute_IdAttributeType);
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 0, CellType = CellType.String)]
        public string NameAttributeType
        {
            get { return proxyItem.Name_AttributeType; }
            set
            {
                proxyItem.Name_AttributeType = value;
                RaisePropertyChanged(NotifyChanges.OItemList_Attribute_NameAttributeType);
            }
        }

        public string IdClass
        {
            get { return proxyItem.ID_Class; }
            set
            {
                proxyItem.ID_Class = value;
                RaisePropertyChanged(NotifyChanges.OItemList_Attribute_IdClass);
            }
        }

        public string NameClass
        {
            get { return proxyItem.Name_Class; }
            set
            {
                proxyItem.Name_Class = value;
                RaisePropertyChanged(NotifyChanges.OItemList_Attribute_NameClass);
            }
        }

        public string IdObject
        {
            get { return proxyItem.ID_Object; }
            set
            {
                proxyItem.ID_Object = value;
                RaisePropertyChanged(NotifyChanges.OItemList_Attribute_IdObject);
            }
        }

        public string NameObject
        {
            get { return proxyItem.Name_Object; }
            set
            {
                proxyItem.Name_Object = value;
                RaisePropertyChanged(NotifyChanges.OItemList_Attribute_NameObject);
            }
        }

        public string IdDataType
        {
            get { return proxyItem.ID_DataType; }
            set
            {
                proxyItem.ID_DataType = value;
                RaisePropertyChanged(NotifyChanges.OItemList_Attribute_IdDataType);
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 1, CellType = CellType.String)]
        public string NameDataType
        {
            get { return proxyItem.Name_DataType; }
            set
            {
                proxyItem.Name_DataType = value;
                RaisePropertyChanged(NotifyChanges.OItemList_Attribute_NameDataType);
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 2, CellType = CellType.Integer)]
        public long OrderId
        {
            get { return proxyItem.OrderID ?? 0; }
            set
            {
                proxyItem.OrderID = value;
                RaisePropertyChanged(NotifyChanges.OItemList_Attribute_OrderId);
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 3, CellType = CellType.String)]
        public string Value
        {
            get { return proxyItem.Val_Named; }
            set
            {
                proxyItem.Val_Named = value;
                RaisePropertyChanged(NotifyChanges.OItemList_Attribute_ValueNamed);
            }
        }

        public bool? Value_Bit
        {
            get { return proxyItem.Val_Bit; }
            set
            {
                proxyItem.Val_Bit = value;
                RaisePropertyChanged(NotifyChanges.OItemList_Attribute_ValueBool);
            }
        }

        public DateTime? Value_DateTime
        {
            get { return proxyItem.Val_Date; }
            set
            {
                proxyItem.Val_Date = value;
                RaisePropertyChanged(NotifyChanges.OItemList_Attribute_ValueDate);
            }
        }

        public long? Value_Int
        {
            get { return proxyItem.Val_Int; }
            set
            {
                proxyItem.Val_Int = value;
                RaisePropertyChanged(NotifyChanges.OItemList_Attribute_ValueInt);
            }
        }

        public double? Value_Double
        {
            get { return proxyItem.Val_Double; }
            set
            {
                proxyItem.Val_Double = value;
                RaisePropertyChanged(NotifyChanges.OItemList_Attribute_ValueDouble);
            }
        }

        public string Value_String
        {
            get { return proxyItem.Val_String; }
            set
            {
                proxyItem.Val_String = value;
                RaisePropertyChanged(NotifyChanges.OItemList_Attribute_ValueString);
            }
        }

        public clsOntologyItem SaveItem(Globals globals)
        {
            var dbConnectorSave = new OntologyModDBConnector(globals);

            var result = dbConnectorSave.SaveObjAtt(new List<clsObjectAtt> { proxyItem });

            return result;
        }

        [DataViewColumn(IsVisible = false, DisplayOrder = 1, CellType = CellType.String, Width = 250)]
        public string IdItem
        {
            get
            {
                return proxyItem != null ? proxyItem.ID_Object + proxyItem.ID_AttributeType + proxyItem.OrderID : null;
            }
        }


        public clsObjectAtt GetProxyItem()
        {
            return proxyItem;
        }

        public ObjectAttributeViewItem(clsObjectAtt proxyItem)
        {
            this.proxyItem = proxyItem;
        }
    }
}
