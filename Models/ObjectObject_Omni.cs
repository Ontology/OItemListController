﻿using OItemListController.Notifications;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OItemListController.Models
{
    public class ObjectObject_Omni : ObjectRelationViewItem
    {
        [DataViewColumn(IsVisible = true,
                    DisplayOrder = 1, CellType = CellType.String)]
        public string NameObject
        {
            get { return proxyItem.Name_Object; }
            set
            {
                proxyItem.Name_Object = value;
                RaisePropertyChanged(NotifyChanges.OItemList_ObjectRel_Name_Object);
            }
        }

        [DataViewColumn(IsVisible = true,
                    DisplayOrder = 4, CellType = CellType.String)]
        public string NameOther
        {
            get { return proxyItem.Name_Other; }
            set
            {
                proxyItem.Name_Other = value;
                RaisePropertyChanged(NotifyChanges.OItemList_ObjectRel_Name_Other);
            }
        }

        [DataViewColumn(IsVisible = true,
                    DisplayOrder = 3, CellType = CellType.String)]
        public string NameRelationType
        {
            get { return proxyItem.Name_RelationType; }
            set
            {
                proxyItem.Name_RelationType = value;
                RaisePropertyChanged(NotifyChanges.OItemList_ObjectRel_Name_RelationType);
            }
        }

        [DataViewColumn(IsVisible = true,
                    DisplayOrder = 5, CellType = CellType.String)]
        public string NameParentOther
        {
            get { return proxyItem.Name_Parent_Other; }
            set
            {
                proxyItem.Name_Parent_Other = value;
                RaisePropertyChanged(NotifyChanges.OItemList_ObjectRel_Name_Parent_Other);
            }
        }

        [DataViewColumn(IsVisible = true,
                    DisplayOrder = 2, CellType = CellType.String)]
        public string NameParentObject
        {
            get { return proxyItem.Name_Parent_Object; }
            set
            {
                proxyItem.Name_Parent_Object = value;
                RaisePropertyChanged(NotifyChanges.OItemList_ObjectRel_Name_Parent_Object);
            }
        }

        [DataViewColumn(IsVisible = true,
                   DisplayOrder = 0, CellType = CellType.Integer)]
        public long OrderId
        {
            get { return proxyItem.OrderID ?? 0; }
            set
            {
                proxyItem.OrderID = value;
                RaisePropertyChanged(NotifyChanges.OItemList_ObjectRel_OrderID);
            }
        }

        [DataViewColumn(IsVisible = true,
                   DisplayOrder = 6, CellType = CellType.String)]
        public string Ontology
        {
            get { return proxyItem.Ontology; }
            set
            {
                proxyItem.Ontology = value;
                RaisePropertyChanged(NotifyChanges.OItemList_ObjectRel_Ontology);
            }
        }

        [DataViewColumn(IsVisible = false, DisplayOrder = 1, CellType = CellType.String, Width = 250)]
        public string IdItem
        {
            get
            {
                return proxyItem != null ? proxyItem.ID_Object + proxyItem.ID_Other + proxyItem.ID_RelationType : null;
            }
        }

        public ObjectObject_Omni(clsObjectRel proxyItem) : base(proxyItem)
        {

        }
    }
}
