﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OItemListController.Models
{
    public class ResultAttribute
    {
        public bool? ResultBool { get; set; }
        public DateTime? ResultDateTime { get; set; }
        public long? ResultInt { get; set; }
        public double? ResultDouble { get; set; }
        public string ResultStr { get; set; }

        public int OrderId { get; set; }

    }
}
