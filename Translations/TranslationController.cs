﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OItemListController.Translations
{
    public class TranslationController
    {

        
        public string Title_FileStreamError
        {
            get
            {
                return GetValue("244d7e5cd2e64401bd99a91ba98fda34", "Datei-Fehler!");
            }

        }

        public string Text_FileStreamError
        {
            get
            {
                return GetValue("5682b2ddf0be4d1babeac59245d25a85", "Datei für Datenaustausch konnte  nicht agespeichert werden!");
            }

        }

        public string Title_WebConfigError
        {
            get
            {
                return GetValue("78634024801646ceba093bb8c62f7f0a", "Webserver-Fehler");
            }

        }

        public string Text_WebConfigError
        {
            get
            {
                return GetValue("7d1199b177ba435b8c2bcb917cec913b", "Der Webserver hat einen Fehler verursacht!");
            }

        }

        public string Label_New
        {
            get
            {
                return GetValue("Label_New", "New");
            }
        }

        public string Label_Apply
        {
            get
            {
                return GetValue("Label_Apply", "Apply");
            }
        }

        public string Label_Guid
        {
            get
            {
                return GetValue("Label_Guid", "Guid");
            }
        }

        public string Label_Save
        {
            get
            {
                return GetValue("Label_Save", "Save");
            }
        }

        public string Label_Name
        {
            get
            {
                return GetValue("Label_Name", "Name");
            }
        }

        public string Label_OrderId
        {
            get
            {
                return GetValue("Label_OrderId", "OrderId");
            }
        }

        public string Text_Check
        {
            get
            {
                return GetValue("Text_Check", "Prüfung...");
            }
        }

        public string Text_AttributeTypeExists
        {
            get
            {
                return GetValue("Text_AttributeTypeExists", "Der Attributtyp existiert!");
            }
        }

        public string Text_RelationTypeExists
        {
            get
            {
                return GetValue("Text_RelationTypeExists", "Der Beziehungstyp existiert!");
            }
        }

        public string Text_InstanceExists
        {
            get
            {
                return GetValue("Text_InstanceExists", "Ein Objekt mit dem Namen existiert.");
            }
        }

        public string GetTranslatedByPropertyName(string propertyName)
        {
            var property = this.GetType().GetProperties().Cast<PropertyInfo>().FirstOrDefault(propItem => propItem.Name == propertyName);

            if (property != null)
            {
                var value = property.GetValue(this);
                return value != null ? value.ToString() : "xxx_Error_xxx";
            }
            else
            {
                return "xxx_Error_xxx";
            }
        }


        private string GetValue(string IdReference, string defaultValue)
        {
            return defaultValue;
        }

    }
}
