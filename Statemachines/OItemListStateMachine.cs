﻿using OntoMsg_Module.StateMachines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OItemListController.Statemachines
{
    [Flags]
    public enum OItemListViewState
    {
        None = 0,
        Initialize = 1,
        ItemListLoadRequest = 2,
        ItemListLoaded = 4
    }
    public class OItemListStateMachine : ControllerStateMachine
    {
        private OItemListViewState viewState;
        public OItemListViewState ViewState
        {
            get { return viewState; }
            private set
            {
                viewState = value;
                RaisePropertyChanged(nameof(ViewState));
            }
        }

        public bool ShowLoader
        {
            get
            {
                if (ViewState.HasFlag(OItemListViewState.Initialize))
                {
                    return true;
                }

                if (ViewState.HasFlag(OItemListViewState.ItemListLoadRequest))
                {
                    return true;
                }


                return false;
            }
        }

        public void Initialize()
        {
            ViewState = OItemListViewState.Initialize;
        }

        public void ItemListLoaded()
        {
            ViewState = OItemListViewState.ItemListLoaded;
        }

        public void ItemListLoadRequest()
        {
            ViewState |= OItemListViewState.ItemListLoadRequest;
        }

        
        public OItemListStateMachine(StateMachineType stateMachineType) : base(stateMachineType)
        {

        }
    }
}
