﻿using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace OItemListController.Factories
{
    public class ObjectTreeFactory : NotifyPropertyChange
    {

        private clsLogStates logStates = new clsLogStates();
        private object asyncLocker = new object();

        private clsOntologyItem resultTreeNodes;
        public clsOntologyItem ResultTreeNodes
        {
            get
            {
                lock (asyncLocker)
                {
                    return resultTreeNodes;
                }

            }
            set
            {
                lock (asyncLocker)
                {
                    resultTreeNodes = value;
                }

                RaisePropertyChanged(nameof(ResultTreeNodes));
            }
        }

        private SessionFile sessionFile;
        public SessionFile SessionFile
        {
            get
            {
                lock (asyncLocker)
                {
                    return sessionFile;
                }

            }
            set
            {
                lock (asyncLocker)
                {
                    sessionFile = value;
                }

            }
        }

        public async Task<clsOntologyItem> WriteTreeNodes(List<clsOntologyItem> objects,  List<clsObjectTree> objectTree, SessionFile sessionFile)
        {
            SessionFile = sessionFile;
            var rootNodes = (from obj in objects
                                 join par in objectTree on obj.GUID equals par.ID_Object into pars
                                 from par in pars.DefaultIfEmpty()
                                 where par == null
                                 select new KendoTreeNode { NodeId = obj.GUID, NodeName = obj.Name }).OrderBy(par => par.NodeName).ToList();

            using (sessionFile.StreamWriter)
            {
                using (var jsonWriter = new Newtonsoft.Json.JsonTextWriter(sessionFile.StreamWriter))
                {
                    jsonWriter.WriteStartObject();
                    jsonWriter.WritePropertyName("data");
                    jsonWriter.WriteStartArray();

                    rootNodes.ForEach(rootNode =>
                    {
                        jsonWriter.WriteStartObject();

                        jsonWriter.WritePropertyName(nameof(KendoTreeNode.NodeId));
                        jsonWriter.WriteValue(rootNode.NodeId);
                        jsonWriter.WritePropertyName(nameof(KendoTreeNode.NodeName));
                        jsonWriter.WriteValue(rootNode.NodeName);
                        WriteSubNodes(rootNode, objects, objectTree, jsonWriter);
                        jsonWriter.WriteEndObject();

                    });

                    jsonWriter.WriteEndArray();
                    jsonWriter.WriteEndObject();
                }
            }

            ResultTreeNodes = logStates.LogState_Success.Clone();
            return ResultTreeNodes;
        }

        private void WriteSubNodes(KendoTreeNode treeNode,  List<clsOntologyItem> objects, List<clsObjectTree> objectTree, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            var nodes = objectTree.Where(node => node.ID_Object_Parent == treeNode.NodeId).OrderBy(node => node.OrderID).ThenBy(node => node.Name_Object).ToList();

            if (nodes.Any())
            {
                jsonWriter.WritePropertyName(nameof(KendoTreeNode.HasChildren));
                jsonWriter.WriteValue(true);
                jsonWriter.WritePropertyName(nameof(KendoTreeNode.SubNodes));
                jsonWriter.WriteStartArray();
                if (nodes.Any())
                {


                    nodes.ForEach(node =>
                    {
                        jsonWriter.WriteStartObject();

                        jsonWriter.WritePropertyName(nameof(KendoTreeNode.NodeId));
                        jsonWriter.WriteValue(node.ID_Object);
                        jsonWriter.WritePropertyName(nameof(KendoTreeNode.NodeName));
                        jsonWriter.WriteValue(node.Name_Object);

                        WriteSubNodes(new KendoTreeNode { NodeId = node.ID_Object, NodeName = node.Name_Object }, objects, objectTree, jsonWriter);

                        jsonWriter.WriteEndObject();


                    });


                }
                jsonWriter.WriteEndArray();
            }
            else
            {
                jsonWriter.WritePropertyName(nameof(KendoTreeNode.HasChildren));
                jsonWriter.WriteValue(false);
            }
            

        }
    }
}
