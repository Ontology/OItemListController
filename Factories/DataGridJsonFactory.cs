﻿using Newtonsoft.Json;
using OItemListController.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OItemListController.Factories
{
    public class DataGridJsonFactory<TTYPE>
    {
        public List<TTYPE> FilterList { get; set; }
        public string FilterPropertyName { get; set; }

        public bool CreateTableHtmlOfModel(StreamWriter streamWriter)
        {
            var propertyAttribs = typeof(TTYPE).GetProperties().Select(propItem =>
            {
                var attribute = (DataViewColumnAttribute)propItem.GetCustomAttributes(true).FirstOrDefault(attrib => attrib.GetType() == typeof(DataViewColumnAttribute));

                if (attribute == null) return null;

                return new { PropertyItem = propItem, AttributeItem = attribute };
            }).Where(propAtt => propAtt != null).OrderBy(propAtt => propAtt.AttributeItem.DisplayOrder).ToList();


            using (var jsonWriter = new JsonTextWriter(streamWriter))
            {
                
                    

                var tableTag = new System.Web.UI.WebControls.Table();
                tableTag.ID = "grid";
                var tableHeader = new System.Web.UI.WebControls.TableRow();
                foreach(var propAtt in propertyAttribs)
                {
                    var tableHeaderCell = new System.Web.UI.WebControls.TableHeaderCell();
                    var tableCell = new System.Web.UI.WebControls.TableCell();

                    tableHeader.Cells.Add(tableHeaderCell);
                    tableHeaderCell.Text = propAtt.PropertyItem.Name;

                }
                tableTag.Rows.Add(tableHeader);
                StringBuilder sb = new StringBuilder();
                using (System.Web.UI.HtmlTextWriter tw = new System.Web.UI.HtmlTextWriter(new StringWriter(sb)))
                {
                    System.Web.UI.WebControls.DataGrid htmlTable = new System.Web.UI.WebControls.DataGrid();
                    tableTag.RenderControl(tw);
                }

                jsonWriter.WriteStartObject();
                jsonWriter.WritePropertyName("html");
                jsonWriter.WriteValue(System.Web.HttpUtility.HtmlEncode(sb.ToString()));
                jsonWriter.WriteEndObject();
            }


            return true;
        }

        public bool CreateEmptyData(StreamWriter streamWriter)
        {
            var propertyAttribs = typeof(TTYPE).GetProperties().Select(propItem =>
            {
                var attribute = (DataViewColumnAttribute)propItem.GetCustomAttributes(true).FirstOrDefault(attrib => attrib.GetType() == typeof(DataViewColumnAttribute));

                if (attribute == null) return null;

                return new { PropertyItem = propItem, AttributeItem = attribute };
            }).Where(propAtt => propAtt != null).OrderBy(propAtt => propAtt.AttributeItem.DisplayOrder).ToList();

            using (var jsonWriter = new JsonTextWriter(streamWriter))
            {
                jsonWriter.WriteStartObject();
                jsonWriter.WritePropertyName("data");
                jsonWriter.WriteStartArray();
                jsonWriter.WriteStartArray();

                propertyAttribs.ForEach(propAtt =>
                {

                    //jsonWriter.WritePropertyName(propAtt.AttributeItem.Caption == null ? propAtt.PropertyItem.Name : propAtt.AttributeItem.Caption);
                    if (propAtt.PropertyItem.PropertyType == typeof(string))
                    {
                        jsonWriter.WriteValue(string.Empty);
                    }
                    else
                    {
                        if (propAtt.PropertyItem.PropertyType == typeof(bool))
                        {
                            bool? value = null;
                            jsonWriter.WriteValue(value);
                        }
                        else if (propAtt.PropertyItem.PropertyType == typeof(int))
                        {
                            int? value = null;
                            jsonWriter.WriteValue(value);
                        }
                        else if (propAtt.PropertyItem.PropertyType == typeof(double))
                        {
                            double? value = null;
                            jsonWriter.WriteValue(value);
                        }
                        else if (propAtt.PropertyItem.PropertyType == typeof(DateTime))
                        {
                            DateTime? value = null;
                            jsonWriter.WriteValue(value);
                        }

                    }

                });

                jsonWriter.WriteEndArray();
                
                jsonWriter.WriteEndArray();
                jsonWriter.WriteEndObject();
            }

            return true;
        }
        
        public bool CreateGridColumnsJsonOfModel(StreamWriter streamWriter)
        {
            var propertyAttribs = typeof(TTYPE).GetProperties().Select(propItem =>
            {
                var attribute = (DataViewColumnAttribute)propItem.GetCustomAttributes(true).FirstOrDefault(attrib => attrib.GetType() == typeof(DataViewColumnAttribute));

                if (attribute == null) return null;

                return new { PropertyItem = propItem, AttributeItem = attribute };
            }).Where(propAtt => propAtt != null).OrderBy(propAtt => propAtt.AttributeItem.DisplayOrder).ToList();

            
            using (var jsonWriter = new JsonTextWriter(streamWriter))
            {

                jsonWriter.WriteStartObject();

                jsonWriter.WritePropertyName("columns");

                jsonWriter.WriteStartArray();

                propertyAttribs.ForEach(propAtt =>
                {
                    jsonWriter.WriteStartObject();

                    jsonWriter.WritePropertyName("name");
                    jsonWriter.WriteValue(propAtt.PropertyItem.Name);

                    jsonWriter.WritePropertyName("label");
                    jsonWriter.WriteValue(propAtt.AttributeItem.Caption != null ? propAtt.AttributeItem.Caption : propAtt.PropertyItem.Name);

                    jsonWriter.WritePropertyName("editable");
                    jsonWriter.WriteValue(!propAtt.AttributeItem.IsReadonly);

                    jsonWriter.WritePropertyName("cell");
                    jsonWriter.WriteValue(propAtt.AttributeItem.CellType.ToString());

                    jsonWriter.WritePropertyName("renderable");
                    jsonWriter.WriteValue(propAtt.AttributeItem.IsVisible);

                    jsonWriter.WriteEndObject();
                });

                jsonWriter.WriteEndArray();
                jsonWriter.WriteEndObject();
            }
            return true;
        }

        public bool CreateGridDataJsonOfModel(List<TTYPE> itemList,StreamWriter streamWriter, bool isVisibleApply)
        {
            
            using (var jsonWriter = new JsonTextWriter(streamWriter))
            {
                var propertyAttribs = typeof(TTYPE).GetProperties().Select(propItem =>
                {
                    var attribute = (DataViewColumnAttribute)propItem.GetCustomAttributes(true).FirstOrDefault(attrib => attrib.GetType() == typeof(DataViewColumnAttribute));

                    if (attribute == null) return null;

                    return new { PropertyItem = propItem, AttributeItem = attribute };
                }).Where(propAtt => propAtt != null).OrderBy(propAtt => propAtt.AttributeItem.DisplayOrder).ToList();

                jsonWriter.WriteStartArray();
                itemList.ForEach(item =>
                {
                    jsonWriter.WriteStartObject();

                    propertyAttribs.ForEach(propAtt =>
                    {
                        
                        jsonWriter.WritePropertyName(propAtt.AttributeItem.Caption == null ? propAtt.PropertyItem.Name : propAtt.AttributeItem.Caption);
                        if (propAtt.PropertyItem.PropertyType == typeof(string))
                        {
                            jsonWriter.WriteValue(propAtt.PropertyItem.GetValue(item));
                        }
                        else
                        {
                            jsonWriter.WriteValue(propAtt.PropertyItem.GetValue(item));
                        }

                    });

                    if (isVisibleApply)
                    {
                        jsonWriter.WritePropertyName("ItemApply");
                        jsonWriter.WriteValue(false);
                    }
                    jsonWriter.WriteEndObject();
                });

                
                jsonWriter.WriteEndArray();
                
                
            }

            return true;
        }
    }
}
