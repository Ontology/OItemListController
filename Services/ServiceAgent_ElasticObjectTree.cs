﻿using ImportExport_Module.Base;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OItemListController.Services
{
    public class ServiceAgent_ElasticObjectTree : NotifyPropertyChange
    {
        private object asyncLocker = new object();

        private clsRelationConfig relationConfig;
        private clsTransaction transaction;

        private clsLocalConfig localConfig;

        private clsOntologyItem resultFlatItems;
        public clsOntologyItem ResultFlatItems
        {
            get
            {
                lock (asyncLocker)
                {
                    return resultFlatItems;
                }

            }
            set
            {
                lock (asyncLocker)
                {
                    resultFlatItems = value;
                }

                RaisePropertyChanged(nameof(ResultFlatItems));
            }
        }

        private List<clsOntologyItem> flatItems;
        public List<clsOntologyItem> FlatItems
        {
            get
            {
                lock (asyncLocker)
                {
                    return flatItems;
                }

            }
            set
            {
                lock (asyncLocker)
                {
                    flatItems = value;
                }
                RaisePropertyChanged(nameof(FlatItems));
            }
        }

        private clsOntologyItem resultRelationType;
        public clsOntologyItem ResultRelationType
        {
            get
            {
                lock (asyncLocker)
                {
                    return resultRelationType;
                }

            }
            set
            {
                lock (asyncLocker)
                {
                    resultRelationType = value;
                }

                RaisePropertyChanged(nameof(ResultRelationType));
            }
        }

        private clsOntologyItem relationType;
        public clsOntologyItem RelationType
        {
            get
            {
                lock (asyncLocker)
                {
                    return relationType;
                }

            }
            set
            {
                lock (asyncLocker)
                {
                    relationType = value;
                }
                RaisePropertyChanged(nameof(RelationType));
            }
        }

        private clsOntologyItem resultRelationTypes;
        public clsOntologyItem ResultRelationTypes
        {
            get
            {
                lock(asyncLocker)
                {
                    return resultRelationTypes;
                }
                
            }
            set
            {
                lock (asyncLocker)
                {
                    resultRelationTypes = value;
                }
                
                RaisePropertyChanged(nameof(ResultRelationTypes));
            }
        }

        private List<clsOntologyItem> relationTypes;
        public List<clsOntologyItem> RelationTypes
        {
            get
            {
                lock(asyncLocker)
                {
                    return relationTypes;
                }
                
            }
            set
            {
                lock (asyncLocker)
                {
                    relationTypes = value;
                }
                RaisePropertyChanged(nameof(RelationTypes));
            }
        }

        private List<clsOntologyItem> relationTypesFiltered;
        public List<clsOntologyItem> RelationTypesFiltered
        {
            get
            {
                lock (asyncLocker)
                {
                    return relationTypesFiltered;
                }

            }
            set
            {
                lock (asyncLocker)
                {
                    relationTypesFiltered = value;
                }
                RaisePropertyChanged(nameof(RelationTypesFiltered));
            }
        }

        private clsOntologyItem resultObjectTree;
        public clsOntologyItem ResultObjectTree
        {
            get
            {
                lock (asyncLocker)
                {
                    return resultObjectTree;
                }

            }
            set
            {
                lock (asyncLocker)
                {
                    resultObjectTree = value;
                }

                RaisePropertyChanged(nameof(ResultObjectTree));
            }
        }

        private List<clsObjectTree> objectTree;
        public List<clsObjectTree> ObjectTree
        {
            get
            {
                lock (asyncLocker)
                {
                    return objectTree;
                }

            }
            set
            {
                lock (asyncLocker)
                {
                    objectTree = value;
                }
                RaisePropertyChanged(nameof(ObjectTree));
            }
        }

        private List<clsOntologyItem> objects;
        public List<clsOntologyItem> Objects
        {
            get
            {
                lock (asyncLocker)
                {
                    return objects;
                }

            }
            set
            {
                lock (asyncLocker)
                {
                    objects = value;
                }
                RaisePropertyChanged(nameof(Objects));
            }
        }

        public clsOntologyItem GetOItem(string id, string type)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);
            return dbReader.GetOItem(id, type);
        }

        public async Task<List<clsOntologyItem>> GetRelationTypes()
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);
            var result = dbReader.GetDataRelationTypes(null, false);

            RelationTypes = dbReader.RelationTypes;
            ResultRelationTypes = result;

            return RelationTypes;
        }

        public async Task<List<clsOntologyItem>> GetFlatItems(clsOntologyItem oItemClass)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var result = dbReader.GetDataObjects(new List<clsOntologyItem> { new clsOntologyItem
                {
                    GUID_Parent = oItemClass.GUID
                }
            });

            FlatItems = dbReader.Objects1;
            ResultFlatItems = result;

            return FlatItems;
        }

        public async Task<clsOntologyItem> GetRelationType(clsOntologyItem oItemClass)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var result = dbReader.GetDataObjectRel(new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Parent_Object = oItemClass.GUID,
                    ID_Parent_Other = oItemClass.GUID
                }
            }, doIds: true);

            clsOntologyItem relationType = null;

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                var relationTypeItems = dbReader.ObjectRelsId.GroupBy(rel => new { id = rel.ID_RelationType, name = rel.Name_RelationType }).Select(grp => new { Id = grp.Key.id, Name = grp.Key.name, Count = grp.Count() }).OrderByDescending(itm => itm.Count);

                if (relationTypeItems != null)
                {
                    relationType = new clsOntologyItem();
                    relationType.GUID = relationTypeItems.First().Id;
                    relationType.Name = relationTypeItems.First().Name;
                    relationType.Type = localConfig.Globals.Type_RelationType;
                }

                RelationTypesFiltered = (from relType in RelationTypes
                                         join relTypeFilter in relationTypeItems on relType.GUID equals relTypeFilter.Id
                                         select relType).ToList();
            }
            RelationType = relationType;
            ResultRelationType = result;

            return RelationType;
        }

        public async Task<List<clsObjectTree>> GetObjectTree(clsOntologyItem oItemClass, clsOntologyItem oItemRelationType)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var result = dbReader.GetDataObjectsTree(oItemClass, oItemClass, oItemRelationType);




            ObjectTree = dbReader.ObjectTree;
            Objects = dbReader.Objects1;
            ResultObjectTree = result;

            
            return ObjectTree;
        }

        public clsOntologyItem SaveSubItems(clsOntologyItem parentObject, List<clsOntologyItem> subObjects, clsOntologyItem relationType, bool asc)
        {
            var firstSubItem = subObjects.FirstOrDefault();
            var dbWriter = new OntologyModDBConnector(localConfig.Globals);

            if (firstSubItem == null) return localConfig.Globals.LState_Nothing.Clone();

            long orderId = 1;
            if (asc)
            {
                orderId = dbWriter.GetDataRelOrderId(parentObject, new clsOntologyItem { GUID_Parent = parentObject.GUID_Parent }, RelationType, false);
                orderId++;
            }

            var firstRel = relationConfig.Rel_ObjectRelation(parentObject, firstSubItem, relationType, asc);
            firstRel.OrderID = orderId;
            var relations = new List<clsObjectRel> { firstRel };

            for (var i = 1; i < subObjects.Count; i++)
            {
                if (asc)
                {
                    orderId += i;
                }

                relations.Add(relationConfig.Rel_ObjectRelation(parentObject, subObjects[i], relationType, false, orderId));
            }

            if (relations.Any())
            {
                
                return dbWriter.SaveObjRel(relations);
            }
            else
            {
                return localConfig.Globals.LState_Nothing;
            }

        }

        public ServiceAgent_ElasticObjectTree(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;

            Initialize();
        }

        public void Initialize()
        {
            relationConfig = new clsRelationConfig(localConfig.Globals);
            transaction = new clsTransaction(localConfig.Globals);
        }
    }

}
