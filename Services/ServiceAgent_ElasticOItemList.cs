﻿using OItemListController.Notifications;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OItemListController.Services
{
    public class ServiceAgent_ElasticOItemList : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;
        private OntologyModDBConnector dbReaderObjects;
        private OntologyModDBConnector dbWriterObjectRel;
        

        private clsOntologyItem resultData_Objects;
        public clsOntologyItem ResultData_Objects
        {
            get { return resultData_Objects; }
            set
            {
                resultData_Objects = value;
                RaisePropertyChanged(NotifyChanges.Elastic_ResultData_Objects);
            }
        }

        private clsOntologyItem resultDelete_Items;
        public clsOntologyItem ResultDelete_Items
        {
            get { return resultDelete_Items; }
            set
            {
                resultDelete_Items = value;
                RaisePropertyChanged(nameof(ResultDelete_Items));
            }
        }


        private Thread thread_GetData;

        public clsOntologyItem GetOItem(string idItem, string typeItem)
        {
            return dbReaderObjects.GetOItem(idItem, typeItem);
        }

        public async Task<clsOntologyItem> DeleteObjectRels(List<clsObjectRel> itemsToDelete)
        {
            var dbDeletor = new OntologyModDBConnector(localConfig.Globals);

            var result = dbDeletor.DelObjectRels(itemsToDelete);

            ResultDelete_Items = result;
            return result;
        }

        public clsOntologyItem GetDataObjects(clsOntologyItem objectFilter = null)
        {
            try
            {
                if (thread_GetData != null)
                {
                    thread_GetData.Abort();
                }
            }
            catch (Exception ex) { }

            thread_GetData = new Thread(GetDataAsync);
            thread_GetData.Start();

            return localConfig.Globals.LState_Success.Clone();
        }

        private void GetDataAsync()
        {
            var result = localConfig.Globals.LState_Success.Clone();

          

            ResultData_Objects = result;
        }

        public clsOntologyItem SaveRelationItem(clsObjectRel relationItem)
        {
            return dbWriterObjectRel.SaveObjRel(new List<clsObjectRel> { relationItem });
            
        }

        public clsOntologyItem IsExisting(string nameInstance, string idClass)
        {
            var dbReaderExisting = new OntologyModDBConnector(localConfig.Globals);

            var searchInstance = new clsOntologyItem
            {
                Name = nameInstance,
                GUID_Parent = idClass
            };

            var result = dbReaderExisting.GetDataObjects(new List<clsOntologyItem> { searchInstance });

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                var existingItem = dbReaderExisting.Objects1.FirstOrDefault(obj => obj.Name.ToLower() == nameInstance.ToLower());

                if (existingItem != null)
                {
                    result = localConfig.Globals.LState_Exists.Clone();
                    result.add_OItem(existingItem);
                    return result;
                }
                else
                {
                    return localConfig.Globals.LState_Nothing.Clone();
                }
            }

            return result;

        }

        public clsOntologyItem IsExisting(string nameItem, bool isAttributeType)
        {
            var dbReaderExisting = new OntologyModDBConnector(localConfig.Globals);

            var searchInstance = new clsOntologyItem
            {
                Name = nameItem
            };

            var result = localConfig.Globals.LState_Success.Clone();

            if (isAttributeType)
            {
                result = dbReaderExisting.GetDataAttributeType(new List<clsOntologyItem> { searchInstance });
            }
            else
            {
                result = dbReaderExisting.GetDataRelationTypes(new List<clsOntologyItem> { searchInstance });
            }
            

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                clsOntologyItem existingItem = null;
                if (isAttributeType)
                {
                    existingItem = dbReaderExisting.AttributeTypes.FirstOrDefault(obj => obj.Name.ToLower() == nameItem.ToLower());
                }
                else
                {
                    existingItem = dbReaderExisting.RelationTypes.FirstOrDefault(obj => obj.Name.ToLower() == nameItem.ToLower());
                }
                

                if (existingItem != null)
                {
                    result = localConfig.Globals.LState_Exists.Clone();
                    result.add_OItem(existingItem);
                    return result;
                }
                else
                {
                    return localConfig.Globals.LState_Nothing.Clone();
                }
            }

            return result;
        }

        public clsOntologyItem SaveItem(clsOntologyItem itemToSave)
        {
            var dbWriter = new OntologyModDBConnector(localConfig.Globals);

            if (itemToSave.Type == localConfig.Globals.Type_AttributeType)
            {
                return dbWriter.SaveAttributeTypes(new List<clsOntologyItem> { itemToSave });
            }
            else if (itemToSave.Type == localConfig.Globals.Type_RelationType)
            {
                return dbWriter.SaveRelationTypes(new List<clsOntologyItem> { itemToSave });
            }
            else if (itemToSave.Type == localConfig.Globals.Type_Object)
            {
                return dbWriter.SaveObjects(new List<clsOntologyItem> { itemToSave });
            }
            else
            {
                return localConfig.Globals.LState_Error.Clone();
            }
        }

        public ServiceAgent_ElasticOItemList(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;

            Initialize();
        }

        private void Initialize()
        {
            dbReaderObjects = new OntologyAppDBConnector.OntologyModDBConnector(localConfig.Globals);
            dbWriterObjectRel = new OntologyModDBConnector(localConfig.Globals);
        }
    }
}
